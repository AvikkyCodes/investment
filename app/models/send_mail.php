<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

Class SendMail extends Model
{
	
  	protected static function sendEmail($receiver, $sub, $bdy)
  	{

        $mail_status = [];
        $sendStatus = false;
        $name = HOST_NAME;  // Name of your website or yours
        $to = $receiver;  // mail of reciever
        $subject = $sub;
        $body = $bdy;
        $from = SUPPORT_EMAIL; // you mail
        $password = SUPPORT_PASSWORD;  // your mail password

        // Ignore from here

        
        $mail = new PHPMailer();

        // To Here

        //SMTP Settings
        $mail->isSMTP();
        // $mail->SMTPDebug = 3;  Keep It commented this is used for debugging                          
        $mail->Host = SMTP_HOST; // smtp address of your email
        $mail->SMTPAuth = true;
        $mail->Username = SUPPORT_EMAIL;
        $mail->Password = $password;
        $mail->Port = MAIL_PORT;  // port
        $mail->SMTPSecure = "465";  // tls or ssl
        $mail->smtpConnect([
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            ]
        ]);

        //Email Settings
        $mail->isHTML(true);
        $mail->setFrom($from, $name);
        $mail->addAddress($to); // enter email address whom you want to send
        $mail->Subject = ("$subject");
        $mail->Body = $body;
        if ($mail->send()) 
        {
            $sendStatus = true;
            $mail_status = ['status' => $sendStatus, 'message' => 'Mail sent successfully.'];
            SendMail::create([
                'sender' => SUPPORT_EMAIL,
                'receiver' => $receiver,
                'subject' => $subject,
                'body'=> $body,
                'date_created' => logTimeStamp()
            ]);
        } 
        else 
        {
            $sendStatus = false;
            $mail_status = ['status' => $sendStatus, 'message' => 'Something is wrong: <br><br>' . $mail->ErrorInfo];
        }

        return $mail_status;
  	}



	//relationships
	


}


?>