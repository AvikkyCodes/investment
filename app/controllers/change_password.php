<?php

Class Change_Password extends Controller
{

	public function index($currentPassword="", $newPassword="", $rNewPassword="")
	{
		$message = ''; $message_type = ERROR_MESSAGE;

		try 
		{
			if (!empty($newPassword) && !empty($currentPassword) && !empty($rNewPassword)) 
			{
				if ($newPassword !== $currentPassword) 
				{
					if ($this->validateCredentials($currentPassword, User::getPassword(getUserId())))
					{		
						if ($newPassword == $rNewPassword) 
						{
							User::changePassword(getUserId(), $newPassword);
							registerLog(ACTIVITY_LOG, getUserEmail() . ' changed password');
							resetUserSession();
							$message = 'Password updated successfully'; 
							$message_type = SUCCESS_MESSAGE;
						} 
						else 
						{
							$message = 'Passwords do not match.';
						}
					}
					else
					{
						$message = 'Current password is incorrect.';
					}
				}
				else
				{
					$message = 'New password cannot be the same as curent password.';
				}
			}
			else
			{
				$message = 'Fill in all required fields.';
			}
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}

		setViewMessage($message, $message_type);
		redirectTo('account-settings');
	}
	
} 

