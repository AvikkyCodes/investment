<?php

    class About extends Controller
    {

        public function index()
        {  
            if (is_logged_in()) 
            {
                redirectTo('dashboard');
            }
            else
            {
                $this->view("about", 
                    [
                        'message' => '',
                        'message_type' => ''
                    ]
                );
            }
        }
        

    }

?>