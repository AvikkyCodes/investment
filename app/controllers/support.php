<?php

Class Support extends Controller
{

	public function index()
	{
		
        $msg = getViewMessage();

		$this->view('support',
					[
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

