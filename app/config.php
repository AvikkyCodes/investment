<?php

//app root directory
define('APP_ROOT', 				__DIR__);

define('HOST_NAME', 'Henley Trade Global');
define('SMTP_HOST', 'henleytradeglobal.com');
define('SUPPORT_EMAIL', 'support@henleytradeglobal.com');
define('SUPPORT_PASSWORD', 'Support@2022');
define('MAIL_PORT', 587);

//user permissions
define('FULL_CONTROL', 			4); //**+ create, update and delete users
define('READ_WRITE_MODIFY', 	3); //view, create, update and delete
define('READ_WRITE', 			2); //view and update
define('READ_ONLY', 			1); //view only


//message types for views
define('SUCCESS_MESSAGE', 		'alert alert-success');
define('ERROR_MESSAGE', 		'alert alert-danger');
define('WARNING_MESSAGE', 		'alert alert-warning');
define('INFO_MESSAGE', 			'alert alert-info');


//logs
define('ACTIVITY_LOG', 			'activity');
define('ERROR_LOG', 			'error');
define('WARNING_LOG', 			'warning');



//post routes
define('POST_ROUTES', [
    'do_login', 'do_register', 'change_password', 'do_deposit', 'do_fund', 'do_jp',
    'do_transfer', 'do_withdraw', 'exchange_crypto', 'i_details', 'init_deposit',
    'process_deposit', 'process_withdrawal', 'send_code', 'send_message', 'simulate_transaction',
    'update_settings', 'update_user',
]);

//non validated routes
define('NO_VALIDATE', [
    'index', 'login', 'do_login', 'logout', 'register', 'do_register', 'email_verification',
    'confirm_email', 'forgot_password', 'reset_password', 'contact', 'faq', 'privacy', 'about',
    'send_code', 'verify_email', 'referrals', 'recent_transactions'
]);



?>