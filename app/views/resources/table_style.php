<style type="text/css">
    .progress {
      height: 50px;
      border: 5px solid white;
      border-radius: 15px;
    }

    #mytable {
      padding: 15px;
      margin: 10px;
      text-align: center;
      vertical-align: middle;
    }

    .con-table {
      /*max-height: 600px;*/
      padding: 20px 20px 50px 20px;
    }

    .con-table table {
      padding: 20px;
    }

    #table-link {
      text-decoration: underline;
    }

 </style>