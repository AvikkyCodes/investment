<?php

Class Process_Multiple extends Controller
{

	public function withdrawal()
	{
        if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
        
        if (!checkViewData('multiple')) 
		{ redirectTo('dashboard'); }

        $data = getViewData('multiple');
		$message = ''; $message_type = SUCCESS_MESSAGE;
        
		try 
		{
            if ($data->action == 'approve') 
            {
                for ($i=0; $i < count($data->selected); $i++) 
                { 
                    $exp = explode(",", $data->selected[$i]);
                    $id = (int) $exp[0];
                    $amount = (int) $exp[1];
                    $withdrawal = UserWithdrawal::find($id);
                    UserWithdrawal::approveWithdrawal($id);
                    UserAccount::updateAccountBalance('debit', $withdrawal['user_id'], $amount);
                    RecentTransaction::makeTransaction('withdrawal', User::getFullName($withdrawal['user_id']) , $amount);
                    registerLog(ACTIVITY_LOG, 'Withdrawal of $' .number_format($amount).' from '. User::getEmail($withdrawal['user_id']) .' approved.');
                }
                $message = 'Withdrawals approved successfully.';
            } 
            else 
            {
                for ($i=0; $i < count($data->selected); $i++) 
                { 
                    $exp = explode(",", $data->selected[$i]);
                    $id = (int) $exp[0];
                    $amount = (int) $exp[1];
                    $withdrawal = UserWithdrawal::find($id);
                    UserWithdrawal::declineWithdrawal($id, $data->reason);
                    if ($withdrawal['status'] !== 0) 
                    {
                        UserAccount::updateAccountBalance('credit', $withdrawal['user_id'], $amount);
                    }
                    registerLog(ACTIVITY_LOG, 'User ('.User::getEmail($withdrawal['user_id']).') withdrawal of $' .number_format($amount).' declined.');
                }
                $message = 'Withdrawals declined successfully.';
            }
            
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo('user-withdrawals');
	}


    public function deposit()
    {
        if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
        
        if (!checkViewData('multiple')) 
		{ redirectTo('dashboard'); }

        $data = getViewData('multiple');
		$message = ''; $message_type = SUCCESS_MESSAGE;
        
		try 
		{
            if ($data->action == 'approve') 
            {
                for ($i=0; $i < count($data->selected); $i++) 
                { 
                    $exp = explode(",", $data->selected[$i]);
                    $id = (int) $exp[0];
                    $amount = (int) $exp[1];
                    $deposit = UserDeposit::find($id);
                    UserDeposit::approveDeposit($id);
                    UserAccount::updateAccountBalance('credit', $deposit['user_id'], $amount);
                    RecentTransaction::makeTransaction('deposit', User::getFullName($deposit['user_id']) , $amount);
                    registerLog(ACTIVITY_LOG, 'Deposit of $' .number_format($amount).' from '. User::getEmail($deposit['user_id']) .' approved.');
                }
                $message = 'Deposits approved successfully.';
            } 
            else 
            {
                for ($i=0; $i < count($data->selected); $i++) 
                { 
                    $exp = explode(",", $data->selected[$i]);
                    $id = (int) $exp[0];
                    $amount = (int) $exp[1];
                    $deposit = UserDeposit::find($id);
                    UserDeposit::declineDeposit($id, $data->reason);
                    if ($deposit['status'] !== 0) 
                    {
                        UserAccount::updateAccountBalance('debit', $deposit['user_id'], $amount);
                    }
                    registerLog(ACTIVITY_LOG, 'User ('.User::getEmail($deposit['user_id']).') deposit of $' .number_format($amount).' declined.');
                }
                $message = 'Deposits declined successfully.';
            }
            
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo('user-deposits');
    }
	
} 

