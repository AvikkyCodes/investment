<?php

Class Update_Settings extends Controller 
{

	public function index($full_name = "", $email = "", $phone = "", $dob="", $address="")
	{
        $message = 'Profile info updated successfully'; 
        $message_type = SUCCESS_MESSAGE;

		try
		{
			User::updatePersonalInfo(getUserId(), $full_name, $phone, $address, $dob);
            registerLog(ACTIVITY_LOG, $email . ' updated personal info.');
		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('account-settings');
		
	}

	public function withdrawal($bank_name = "", $account_name = "", $account_number = "", $swift_code="", 
                        $btc_address="", $eth_address="", $ltc_address="")
	{
        $message = 'Withdrawal info updated successfully'; 
        $message_type = SUCCESS_MESSAGE;

		try
		{
			UserSetting::updateWithdrawalInfo(getUserId(), $bank_name, $account_name, $account_number, 
                                             $swift_code, $btc_address, $eth_address, $ltc_address);
            registerLog(ACTIVITY_LOG, $email . ' updated withdrawal info.');
		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('account-settings');
		
	}

	public function wallets($btc_address="", $eth_address="")
	{
        $message = 'Wallet info updated successfully'; 
        $message_type = SUCCESS_MESSAGE;

		try
		{
			PaymentMethod::updateInfo($btc_address, $eth_address);
            registerLog(ACTIVITY_LOG, $email . ' updated wallet info.');
		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('wallets');
		
	}


}