<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
    $page_title = 'Investment Plans'; 
    require_once 'resources/header.php';
  ?>

  <style>
    .feature {
        margin: 5px;
    }
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

   <?php require_once 'resources/sidebar.php'; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
        <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <div class="row">

              <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                        Investment Plans
                    </h1>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    <?php require_once 'resources/form_sub_msg.php';?>
                  
                    <div class="row">
                        <div class="col-md-4">
                        <div class="pricing-table card purple border bg-dark shadow p-4">
								<!-- Table Head -->
								
								<h2 class="text-light">Bronze Plan <br> (1.5% daily for 14 days)</h2>
								<!-- Price -->
								<div class="price-tag mt-2 mb-2 text-center">
									<span class="symbol text-light" style="font-size: 25px;font-weight: bold;">$</span>
									<span class="amount text-light" style="font-size: 50px;font-weight: bold;">100</span>
								</div>
								<!-- Features -->
								<div class="pricing-features">
									<div class="feature text-light">Minimum Possible Deposit :<span class="text-light ml-5">$100</span></div>
									<div class="feature text-light">Maximum Possible Deposit :<span  class="text-light ml-5">$10,000</span></div>
									<div class="feature text-light">Minimum Return :<span class="text-light ml-5">$21</span></div>
									<div class="feature text-light">Maximum Return :<span class="text-light ml-5">$21,000</span></div>
									<div class="feature text-light">Gift Bonus :<span class="text-light ml-5">$0</span></div>
									<div class="feature text-light">Duration :<span class="text-light ml-5">14 Days</span></div>
								</div> <br>
								<!-- Button -->
								<div class="mt-2">
									<form method="post" action="do-jp">
										<h5 class="text-light">Amount to invest : ($100 default)</h5>
										<input type="number" min="100" max="10000" name="amount" placeholder="$100" required class="form-control text-light bg-dark"> <br>
							
										<input type="hidden" name="plan" value="bronze">
										<input type="submit" class="btn btn-block pricing-action btn-primary btn-lg" value="Join plan" >
									</form>
								</div>
							</div>
                        </div>


                        <div class="col-md-4">
                        <div class="pricing-table card purple border bg-dark shadow p-4">
								<!-- Table Head -->
								
								<h2 class="text-light">Standard Plan <br> (2.0% daily for 7 days)</h2>
								<!-- Price -->
								<div class="price-tag mt-2 mb-2 text-center">
									<span class="symbol text-light" style="font-size: 25px;font-weight: bold;">$</span>
									<span class="amount text-light" style="font-size: 50px;font-weight: bold;">500</span>
								</div>
								<!-- Features -->
								<div class="pricing-features">
									<div class="feature text-light">Minimum Possible Deposit :<span class="text-light ml-5">$500</span></div>
									<div class="feature text-light">Maximum Possible Deposit :<span  class="text-light ml-5">$20,000</span></div>
									<div class="feature text-light">Minimum Return :<span class="text-light ml-5">$40</span></div>
									<div class="feature text-light">Maximum Return :<span class="text-light ml-5">$40,000</span></div>
									<div class="feature text-light">Gift Bonus :<span class="text-light ml-5">$0</span></div>
									<div class="feature text-light">Duration :<span class="text-light ml-5">7 Days</span></div>
								</div> <br>
								<!-- Button -->
								<div class="mt-2">
									<form method="post" action="do-jp">
										<h5 class="text-light">Amount to invest : ($500 default)</h5>
										<input type="number" min="500" max="20000" name="amount" placeholder="$500" required class="form-control text-light bg-dark"> <br>
							
										<input type="hidden" name="plan" value="standard">
										<input type="submit" class="btn btn-block pricing-action btn-primary btn-lg" value="Join plan" >
									</form>
								</div>
							</div>
                        </div>


                        

                        <div class="col-md-4">
                            <div class="pricing-table card purple border bg-dark shadow p-4">
								<!-- Table Head -->
								
								<h2 class="text-light">Gold Plan <br> (2.5% daily for 14 days)</h2>
								<!-- Price -->
								<div class="price-tag mt-2 mb-2 text-center">
									<span class="symbol text-light" style="font-size: 25px;font-weight: bold;">$</span>
									<span class="amount text-light" style="font-size: 50px;font-weight: bold;">1,000</span>
								</div>
								<!-- Features -->
								<div class="pricing-features">
									<div class="feature text-light">Minimum Possible Deposit :<span class="text-light ml-5">$1,000</span></div>
									<div class="feature text-light">Maximum Possible Deposit :<span  class="text-light ml-5">$100,000</span></div>
									<div class="feature text-light">Minimum Return :<span class="text-light ml-5">$100</span></div>
									<div class="feature text-light">Maximum Return :<span class="text-light ml-5">$200,000</span></div>
									<div class="feature text-light">Gift Bonus :<span class="text-light ml-5">$0</span></div>
									<div class="feature text-light">Duration :<span class="text-light ml-5">14 Days</span></div>
								</div> <br>
								<!-- Button -->
								<div class="mt-2">
									<form method="post" action="do-jp">
										<h5 class="text-light">Amount to invest : ($1,000 default)</h5>
										<input type="number" min="1000" max="100000" name="amount" placeholder="$1000" required class="form-control text-light bg-dark"> <br>
										
										<input type="hidden" name="paln" value="gold">
										<input type="submit" class="btn btn-block pricing-action btn-primary btn-lg" value="Join plan" >
									</form>
								</div>
							</div>
                        </div>

                        </div>
						</div> 

                    </div>
                    <!-- end of row -->
                  </div>
                 </div>
               </div>
             </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php';
      ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->


</body>

</html>
