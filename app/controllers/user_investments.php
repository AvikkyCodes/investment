<?php

Class User_Investments extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		$msg = getViewMessage();
        $message = $msg['message']; $message_type = $msg['message_type'];
		$investments = UserInvestment::orderBy('id', 'desc')->get();
        
		$this->view('investments',
					[
                        'investments' => $investments,
						'message' => $message,
						'message_type' => $message_type
					]
				);
	}
	
} 

