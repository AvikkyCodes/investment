<?php
  
/**
 * 
 */
class Logout extends Controller
{
	
	public function index()
	{
		if (is_logged_in()) 
		{
			registerLog(ACTIVITY_LOG, $_SESSION['username'] . ' logged out successfully.');

			//do logout process
			require_once APP_ROOT .DS. 'controllers' .DS. 'resources' .DS. 'dologout.php';
		}
		
		closeAllDueInvestments();

		redirectTo('login');
	}

}

?>