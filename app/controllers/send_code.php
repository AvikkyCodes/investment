<?php

Class Send_Code extends Controller
{

	public function index($email="")
	{
        if (empty($email)) 
		{ redirectTo(''); }
		
		$message = 'Verification Email sent successfully. Verify your email to login.'; 
        $message_type = SUCCESS_MESSAGE;
		$redirect = 'email-verification';

		try 
		{
			if (!is_email_sent()) 
			{
				set_email_sent();
			}

			if (email_expired()) 
			{
				$user = User::getByEmail($email);

				if (!$user->email_verified) 
				{
					$verification_code = generate_serial_number(100);
					$verification_link = THIS_SERVER . '/' . 'verify-email/index/' .$email. '/' .$verification_code;
					$expire = generateDays(1);
					$mail_status = SendMail::sendEmail($email, 'EMAIL VERIFICATION',
								'Click on this link to verify your email: ' . $verification_link . '<br> It will expire in 24 hours.');
					
					if ($mail_status['status']) 
					{
						if (EmailVerification::userEmailExists($email)) 
						{
							EmailVerification::updateEmailVerification($email, $verification_code, $expire[1]);
						} 
						else 
						{
							EmailVerification::register($email, $verification_code, $expire[1]);
						}
						email_sent();
					} 
					else 
					{
						$message = $mail_status['message']; 
						$message_type = ERROR_MESSAGE;
					}
				} 
				else 
				{
					$message = 'Email has already been verified. Login to continue.';
					$message_type = ERROR_MESSAGE;
					$redirect = 'login';
				}
			} 
			else 
			{
				$message = 'Verification email can only be requested once every thirty(30) minutes.';
				$message_type = ERROR_MESSAGE;
			}
		
		}
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo($redirect);
	}
	

	public function otp()
	{
        $email = getUserEmail();
		$message = 'OTP sent successfully'; 
        $message_type = SUCCESS_MESSAGE;

		try 
		{
			if (!is_otp_sent()) 
			{
				set_otp_sent();
			}

			if (otp_expired()) 
			{
				$otp = generate_serial_number(6);
				$expire = generateDays(0, 0, 10);
				$mail_status = SendMail::sendEmail($email, 'OTP',
							'This is your OTP (One Time Password): ' . $otp . '<br> It will expire in 10 minutes.');
				
				if ($mail_status['status']) 
				{
					if (EmailVerification::userEmailExists($email)) 
					{
						EmailVerification::updateEmailVerification($email, $otp, $expire[0]);
					} 
					else 
					{
						EmailVerification::register($email, $otp, $expire[0]);
					}
					otp_sent();
				} 
				else 
				{
					$message = $mail_status['message']; 
					$message_type = ERROR_MESSAGE;
				}
			} 
			else 
			{
				$message = 'OTP can only be requested once every ten(10) minutes.';
				$message_type = ERROR_MESSAGE;
			}
			
			
			
		}
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo('withdraw');
	}
	
} 

