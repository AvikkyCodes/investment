<?php

class UserWithdrawal extends Model
{
	protected static function makeWithdrawal($amount, $receiving_mode)
	{
		UserWithdrawal::create([
			'user_id' 					=> getUserId(),
			'amount' 					=> $amount,
			'receiving_mode' 			=> $receiving_mode,
			'date_created'				=> logTimestamp(),
            'remarks'               	=> 'Processing...'
		]);
	}

	protected static function getUserWithdrawals($id)
	{
		$data = UserWithdrawal::where('user_id', '=', $id)->orderBy('date_created', 'desc')->get();
		return $data;
	}

	protected static function getApprovedUserWithdrawals($id)
	{
		$data = UserWithdrawal::where('status', '=', 1)
		->where('user_id', '=', $id)->get();
		return $data;
	}

	protected static function getApprovedWithdrawals()
	{
		$data = UserWithdrawal::where('status', '=', 1)->get();
		return $data;
	}

	protected static function getPendingWithdrawals()
	{
		$data = UserWithdrawal::where('status', '=', 0)->get();
		return $data;
	}

	protected static function approveWithdrawal($id)
	{
		UserWithdrawal::where('id', '=', $id)->update(['status' => 1, 'remarks' => 'Withdrawal approved. Your funds are on the way.']);
	}
	

	protected static function declineWithdrawal($id, $reason)
	{
		UserWithdrawal::where('id', '=', $id)->update(['status' => 2, 'remarks' => $reason]);
	}


	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}