<script type="text/javascript">
//prevent form submission on ENTER KEY
document.getElementById('regForm').addEventListener('keypress', function(event){
  if (event.keyCode == 13) {
    event.preventDefault();
  }
});


var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab


//function to show current tab
function showTab(n) 
{
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  document.getElementById("submitBtn").style.display = "none";
  //... and fix the Previous/Next buttons:
  if (n == 0) 
  {
    document.getElementById("prevBtn").style.display = "none";
  } 
  else 
  {
    document.getElementById("prevBtn").style.display = "inline";
  }
  
  if (n == (x.length - 1)) 
  {
    document.getElementById("nextBtn").style.display = "none";
    document.getElementById("submitBtn").style.display = "inline";
  } 
  else 
  {
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("submitBtn").style.display = "none";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}


// This function will figure out which tab to display
function nextPrev(n) 
{
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;

  showTab(currentTab);
}


// This function deals with validation of the form fields
function validateForm() 
{
  var x, y, z, i, j, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByClassName("form-control");

  //A loop that checks every text field in the current tab:
  for (i = 0; i < y.length; i++) 
  {
    //checking text fields
    if (y[i].value == "") 
    {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) 
  {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}


// This function removes the "active" class of all steps...
function fixStepIndicator(n) 
{
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) 
  {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}



</script>