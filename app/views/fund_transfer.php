<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
    $page_title = 'Transfer Funds'; 
    require_once 'resources/header.php';
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

   <?php require_once 'resources/sidebar.php'; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
        <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <div class="row">

              <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                        User To User Transfer
                    </h1>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                  
                    <div class="row">

                      <div class="col-md-12 ml-3">

                        <?php require_once 'resources/form_sub_msg.php';?>
                        
                          <div class="form-vertical" id="form-div">
                      
                            <form method="POST" class="form-vertical" action="do_transfer" >
                            
                              <div class="form-group">
                              <label>Receiver Email:</label>
                              <input type="email" class="form-control w-50" name="email" placeholder="Enter email address" required
                                >
                              </div>

                              <div class="form-group">
                                <label>Amount to Send:</label>
                                <input type="number" class="form-control w-50" name="amount" placeholder="Enter amount" required
                                >
                              </div>
                    
                              <div class="form-group mr-2 mt-2"> 
                                <input type="submit" class="btn btn-primary form-control w-50 btn-lg" value="Send">
                              </div>

                            </form>
                            
                          </div>

                      </div>

                    </div>
                    <!-- end of row -->
                  </div>
                 </div>
               </div>
             </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php';
      ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->


</body>

</html>
