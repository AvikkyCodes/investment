<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
    $page_title = 'Withdraw Your Funds'; 
    require_once 'resources/header.php';
  ?>

  <style>
    .feature {
        margin: 5px;
    }
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

   <?php require_once 'resources/sidebar.php'; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
        <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <div class="row">

              <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                       Request For Withdrawal
                    </h1>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    <?php require_once 'resources/form_sub_msg.php';?>
                  
                    <div class="row">
                        <div class="col-md-5">
                        <div class="pricing-table card purple border bg-dark shadow p-4">
								<!-- Table Head -->
								
								<!-- <h2 class="text-light">Ethereum</h2> -->
								<!-- Price -->
								<!-- Features -->
								<div class="pricing-features">
									<div class="feature text-light">Minimum Amount :<span class="text-light ml-5">$100</span></div>
									<div class="feature text-light">Maximum Amount :<span  class="text-light ml-5">$1,000,000,000</span></div>
									<div class="feature text-light">Charge Type :<span class="text-light ml-5">Percentage</span></div>
									<div class="feature text-light">Charges Amount :<span class="text-light ml-5">0.2%</span></div>
									
									<div class="feature text-light">Duration :<span class="text-light ml-5">Instant Deposit & Withdrawal</span></div>
								</div> <br>
								
							</div>
                        </div>


                        <div class="col-md-5">
                            
                            <div class="pricing-table card purple border bg-dark shadow p-4">

                                <div class="mb-2 w-50" style="float:right;">
									<form method="post" action="send-code/otp">
										<input type="submit" class="btn btn-block pricing-action btn-success btn-sm" value="Request OTP" >
									</form>
								</div>
								
								<!-- Button -->
								<div class="mt-2">
									<form method="post" action="do-withdraw">
										<h5 class="text-light">Amount to withdraw :</h5>
										<input type="number" min="100" max="1000000000" name="amount" placeholder="$100" required class="form-control text-light bg-dark"> <br>
										
                                        <div class="form-group">
                                            <label class="text-light">Select Receiving Mode:</label>
                                            <div class="form-check inline">
                                            <input class="form-check-input" type="radio" name="mode" value="btc" id="flexRadioDefault1" checked>
                                            <label class="form-check-label text-lg text-bolder text-light" for="flexRadioDefault1">
                                                Bitcoin
                                            </label>
                                            </div>
                                            <div class="form-check mt-2">
                                            <input class="form-check-input" type="radio" name="mode" value="eth" id="flexRadioDefault2">
                                            <label class="form-check-label text-lg text-bolder text-light" for="flexRadioDefault2">
                                                Ethereum
                                            </label>
                                            </div>
                                        </div>

                                        <h5 class="text-light">OTP <small>(Will be sent to your email on request)</small> :</h5>
                                        <input type="text" name="otp" placeholder="Enter OTP" required class="form-control text-light bg-dark"><br>
										<input type="submit" class="btn btn-block pricing-action btn-primary btn-lg" value="Request Withdrawal" >
									</form>
								</div>
							</div>
                        </div>

                        <div class="col-md-2">
                            
                        </div>
                        

                    </div>
                    <!-- end of row -->
                  </div>
                 </div>
               </div>
             </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php';
      ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->


</body>

</html>
