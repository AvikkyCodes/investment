<?php

    class Crypto_Exchange extends Controller
    {

        public function index()
        {  
            $msg = getViewMessage();

            $crypto = UserCrypto::getUserCrypto(getUserId());
            $balance = UserAccount::getAccountBalance(getUserId());

            $this->view("exchange", 
                [
                    'crypto' => $crypto,
                    'balance' => $balance,
                    'message' => $msg['message'],
                    'message_type' => $msg['message_type']
                ]
            );
        }
        

    }

?>