<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
    $page_title = 'Help/Support'; 
    require_once 'resources/header.php';
  ?>

  <style>
    textarea {
      min-height: 150px;
      max-height: 150px;
    }
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

   <?php require_once 'resources/sidebar.php'; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
        <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <div class="row">

              <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                        Henley Trade Global Support
                    </h1>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                  
                    <div class="row">

                      <div class="col-md-12 ml-3">

                        <?php require_once 'resources/form_sub_msg.php';?>

                          <div class="form-vertical" id="form-div">
                            <p class="text-lg text-bolder text-dark">For inquiries, suggestions or complains. Mail us</p>
                            <h2 class="text-primary">support@henleytradeglobal.com</h2>
                            <br>
                      
                            <form method="POST" class="form-vertical" action="send-message" >
                              
                              <div class="form-group">
                                <label class="text-lg text-dark">Message:</label>
                                <textarea class="form-control w-50" name="message" required max="200" min="10"
                                ></textarea>
                              </div>

                              <div class="form-group mr-2 mt-2"> 
                                <button type="submit" class="btn btn-primary btn-lg">Send</button>
                              </div>

                            </form>
                            
                          </div>

                      </div>

                    </div>
                    <!-- end of row -->
                  </div>
                 </div>
               </div>
             </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php';
      ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->


</body>

</html>
