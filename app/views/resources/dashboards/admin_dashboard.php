<!-- Content Row -->
<div class="row">

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Active Users</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                <?=number_format($data['admin_dashboard']['active_users']) ?>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-users fa-2x text-black-300"></i>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Deposits</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                $<?=number_format($data['admin_dashboard']['total_deposits']) ?>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-download fa-2x text-black-300"></i>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Deposits</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"> 
                    <?=number_format($data['admin_dashboard']['pending_deposits']) ?> 
                  </div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-exclamation fa-2x text-black-300"></i>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Pending Withdrawals</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"> 
                    <?=number_format($data['admin_dashboard']['pending_withdrawals']) ?> 
                  </div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-bell fa-2x text-black-300"></i>
            </div>
          </div>
        </div>
      </div>
  </div>
  

</div>

<!-- Content Row -->


<!-- Content Row -->
<div class="row">

<div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Withdrawals</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"> 
                    <?=number_format($data['admin_dashboard']['total_withdrawals']) ?> 
                  </div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-upload fa-2x text-black-300"></i>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Investments</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                    <?=number_format($data['admin_dashboard']['total_investments']) ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-coins fa-2x text-black-300"></i>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  


  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Activity Logs</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"> 
                    <?=number_format($data['admin_dashboard']['logs']) ?> 
                  </div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-list-alt fa-2x text-black-300"></i>
            </div>
          </div>
        </div>
      </div>
  
  </div>

  
</div>

<!-- Content Row -->

