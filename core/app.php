<?php

require "controller.php";

Class App extends Controller
{
	private $controller = 'index';
	private $method = 'index';
	private $params = [];
	public $route = 'index';
	
	
	public function __construct()
	{

		//get the requested url
		$url = $this->parseUrl();

		//if url is valid
		if (isset($url)) 
		{
			if (in_array($url[0], POST_ROUTES) && $_SERVER['REQUEST_METHOD'] !== 'POST') 
			{
				redirectTo('');
				exit();
			}

			//if controller exists
			if (file_exists(APP_ROOT. DS . "controllers" . DS . $url[0] . ".php")) 
			{
				//set controller
				$this->controller = $url[0];
				$this->route = $url[0];
				unset($url[0]);	
			}
			else
			{
				redirectTo('page-not-found');
				exit();
			}
		}

		//require the controller file
		require_once APP_ROOT . DS . "controllers" . DS . $this->controller . ".php";

		//reset controller object
		$this->controller = new $this->controller;

		//if a function is requested
		if (isset($url[1])) 
		{
			//if function exists
			if (method_exists($this->controller, $url[1])) 
			{
				//set requested function
				$this->method = $url[1];
				unset($url[1]);
			}
			else
			{
				redirectTo('page-not-found');
				exit();
			}
		}

		//get parameter values
		//if POST request method
		if ($_SERVER["REQUEST_METHOD"] == "POST") 
		{
			//set params with POST values 
			foreach ($_POST as $key => $value) 
			{
	    		$this->params[$key] = sanitize($value); //sanitize inputed data
			}
			// var_dump($_POST);
			// exit();
		}
		else
		{
			if (isset($url)) 
			{
				//set params with values from url, if any
				$this->params = array_values($url);
			}
		}
		
		//validate user session
		if (!in_array($this->route, NO_VALIDATE)) 
		{
			$this->validateUser($this->route);	
		}

		//establish a database connection
		$this->connectToDatabase();
		

		//invoke requested function in the controller class
		//with the passed parameters
		call_user_func_array([$this->controller, $this->method], 
			$this->params);
	}


	
	//function to convert url into an array
	private function parseUrl()
	{
		if (isset($_GET["url"])) 
		{
			//covert url into array, using '/' as delimeter.
			$url = explode("/", filter_var(fromUrlFriendly(rtrim($_GET["url"]), "/"), FILTER_SANITIZE_URL));
			//sanitize inputed data
			$url = sanitizeArray($url);
			return $url;
		}
	}
	

}