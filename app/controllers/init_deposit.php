<?php

Class Init_Deposit extends Controller
{

	public function index($amount="", $paymentMethod="")
	{
		setViewData([
            'amount' => $amount,
            'paymentMethod' => $paymentMethod
        ], 'payment');

        redirectTo('payment');
	}
	
} 

