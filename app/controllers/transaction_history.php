<?php

Class Transaction_History extends Controller
{

	public function index()
	{
        $message = $message_type = '';
		$deposits = UserDeposit::getUserDeposits(getUserId());
		$transactions = UserTransaction::getUserTransactions(getUserId());
		$withdrawals = UserWithdrawal::getUserWithdrawals(getUserid());

		$this->view('transactions',
					[
                        'deposits' => $deposits,
						'transactions' => $transactions,
						'withdrawals' => $withdrawals,
						'message' => $message,
						'message_type' => $message_type
					]
				);
	}
	
} 

