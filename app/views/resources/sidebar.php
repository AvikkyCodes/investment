<ul class="navbar-nav navbar-fixed-top bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <div class="sidebar-brand d-flex align-items-center justify-content-center">
        <h3 class="sidebar-brand d-flex align-items-center justify-content-center">
          <div  class="sidebar-brand-icon bg-white">
            <img src="../../img/logo.jpeg" class="img-fluid"></i>
          </div>
          <div class="sidebar-brand-text mx-3 text-white">HENLEY</div>
        </h3>
      </div>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="dashboard">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="mb-1">

      <?php

        if (is_an_admin()) 
        {
          require_once 'sidebars/admin_sidebar.php';
        }
        else
        {
          require_once 'sidebars/user_sidebar.php';
        }
  
      ?> 


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">   

      <!-- Sidebar Toggler (Sidebar)  -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
     

</ul>