<?php

//Base controller class that implements
//functions
//which all other controller classes inherit from

Class Controller 

{	
	public $route; 

	//function for rendering views
	protected function view($view, $data = [])
	{
		require_once APP_ROOT .DS. "views" .DS. $view . ".php";
	}


	//validate user session
	protected function validateUser($route)
	{
		$this->route = $route;
		if (is_logged_in()) 
		{
			if (is_inactive()) 
			{
				$this->logout('Session timeout. Login to continue.'
					, INFO_MESSAGE);

				registerLog(ACTIVITY_LOG, getUserUname().' session expired.');
			}
			else
			{
				//validate user permissions
				// $this->validateAccess($route);
				//if user is valid
				//add to user minutes
				add_time();
			}
		} 
		else
		{ 
			//authentication
			$this->logout('Login to continue.', INFO_MESSAGE); 
		}
		
	}



	//authenticate user login credentials and log user in
	protected function login($email, $pwd)
	{
		$id = $username = $password = $full_name = $role = $last_logged_in = $current_logged_in = "";
		$active = false; $account_balance = '';
		
		try 
		{
			//does account exist
			User::where('email', '=', $email)->firstOrFail();

			//fetch user credentials from database
			$userAccount = User::getCredentials($email);
			
			foreach ($userAccount as $user) 
			{
				$id = $user->id;
				$username = $user->username;
				$password = $user->password;
				$full_name = $user->full_name;
				$role = $user->role;
				$active = $user->active;
				$last_logged_in = $user->last_logged_in;
			}

			//is user account disabled
			if ($active == false) 
			{
				$this->loginError('User account is disabled. Please contact support.');
			}

			//validate user password
			if ($this->validateCredentials($pwd, $password)) 
			{
				$account_balance = UserAccount::getAccountBalance($id);
				
				//set @$_session variables
				require_once APP_ROOT .DS. 'controllers' .DS. 'resources' .DS. 'dologin.php';

				//session timeout settings
				$current_logged_in = logTimestamp();

				//set user last login
				$this->setLastLogin($id, $current_logged_in);

				//record  user activity
				registerLog(ACTIVITY_LOG, $email .' '. 'logged in successfully.');

				closeAllDueInvestments();

				//display the landing page
				redirectTo('dashboard');
				
			} 
			else 
			{
				$this->loginError('Username or password incorrect');
			}

		} 
		catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) 
	    {
	       $this->loginError('Username or password incorrect');
	    }
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$this->loginError($msg['message']);
		}		
		
	}

	

	//get required permissions for current route
	protected function getRequiredPermission($route)
	{
		$permission;

		foreach (ROUTE_PERMISSIONS as $key => $value) 
		{ 
			if (in_array($route, $value)) 
			{
				$permission = $key;
			}
		}

		return $permission;
	}


	//does route require permissions
	protected function requiresPermission($route)
	{
		$required = false;

		foreach (ROUTE_PERMISSIONS as $key => $value) 
		{
			if (in_array($route, $value)) 
			{
				$required = true;
			}
		}

		return $required;
	}


	//user access control
	protected function validateAccess($route)
	{
		$valid = false;

		$perms = getUserPerms();

		if ($this->requiresPermission($route)) 
		{
			$requiredPerm = $this->getRequiredPermission($route);

			if (!in_array($requiredPerm, $perms)) 
			{
				redirectTo('access-control');
			}
		}
	}



	//validate user's login credentials
	protected function validateCredentials($pwd, $password)
	{
		//decrypt hashed password
		$decrypted = decryptPassword($pwd, $password);

		return $decrypted ? true : false;
	}


	//update user's last login details
	protected function setLastLogin($id, $timestamp)
	{
		User::setLastLogin($id, $timestamp);
	}


	//login error
	protected function loginError($message)
	{
		setViewMessage($message, ERROR_MESSAGE);
		redirectTo('login');
		exit();
	}


	//forcefully log the user out 
	protected function logout($message, $message_type)
	{
		if (is_logged_in()) 
		{
			require_once APP_ROOT .DS. 'controllers' .DS. 'resources' .DS. 'dologout.php';
		}
		setViewMessage($message, $message_type);
		redirectTo('login');
	}


	//validate submited form using
	//@$_SESSION['csrf_token']
	protected function verifyCsrf($form_name)
	{
		//if the csrf token is not verified
		if (!hash_equals($_SESSION['csrf_token'], $_COOKIE['csrf_token'])) 
		{
			//log this as a warning in the logs table
			registerLog(WARNING_LOG, 'failed to verify CSRF-TOKEN on ' . $form_name . ' form.' );

			//log user out for re-authentication
			$this->logout('Invalid session. Login to continue.', WARNING_MESSAGE);
		} 
		
	}

	protected function connectToDatabase()
	{
		new Database();
	}

	protected function throwError($message)
	{
		setViewMessage($message, ERROR_MESSAGE);
		redirectTo('register');
		exit();
	}

	//validate parameters being passed
	protected function validateParameter($fieldName, $value, $dataType, 
		$required = true) 
	{
		if($required == true && empty($value) == true) {
			return false;
		}

		switch ($dataType) {
			case BOOLEAN:
				if(!is_bool($value)) {
					$this->throwError("Datatype is not valid for " . $fieldName . '. It should be boolean.');
				}
				break;
			case INTEGER:
				if(!is_numeric($value)) {
					$this->throwError("Datatype is not valid for " . $fieldName . '. It should be numeric.');
				}
				break;

			case STRING:
				if(!is_string($value)) {
					$this->throwError("Datatype is not valid for " . $fieldName . '. It should be string.');
				}
				break;
			
			default:
				$this->throwError("Datatype is not valid for " . $fieldName);
				break;
		}

		return sanitize($value);

	}

	
}

