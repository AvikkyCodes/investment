<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'User Referrals';
    require_once 'resources/header.php'; 
    require_once 'resources/table_style.php'; 
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		All User Referrals
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                <?php require_once 'resources/form_sub_msg.php'; ?>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                
                        <thead>
                        <tr id="mytable">
                            <th id="mytable">User</th>
                            <th id="mytable">Referrals</th>
                            <th id="mytable">Referral Bonus ($)</th>
                        </tr>
                        </thead>
  
                        <tbody>
  
                        <?php 
                            foreach ($data['user_referrals'] as $referral):
                        ?>
                            <tr id="mytable">
                                <td id="mytable" class="text-left">
                                    <?=$referral['user']?>
                                </td>
                                <td id="mytable" class="text-left">
                                    <?php foreach ($referral['referrals'] as $ref) : ?>
                                        <ul>
                                            <li><?=$ref['email'] ?></li>
                                        </ul>
                                    <?php endforeach; ?>
                                </td>
                                <td id="mytable" class="text-left">
                                    <?php foreach ($referral['referrals'] as $ref) : ?>
                                        <ul>
                                            <li><?=number_format($ref['referral_bonus'])?></li>
                                        </ul>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                  </div>
                </div>
                    
                  
            </div>
          </div>

          <script>
            function myFunction() {
              /* Get the text field */
              var copyText = document.getElementById("myInput");
              /* Select the text field */
              copyText.select();
              copyText.setSelectionRange(0, 99999); /* For mobile devices */
              /* Copy the text inside the text field */
              document.execCommand("copy");
              /* Alert the copied text */
              alert("Referral link copied: " + copyText.value);
              }
          </script>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>