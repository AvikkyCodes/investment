<?php

Class Do_Deposit extends Controller 
{

	public function index($amount = "", $payment_method = "", $image = "")
	{
        $message = $message_type = '';

		try
		{
			$image_upload_status = ImageUpload::uploadImage('deposits');

			if ($image_upload_status['message'] == 'okay') 
			{
                UserDeposit::makePayment($amount, $payment_method, $image_upload_status['image']);

                registerLog(ACTIVITY_LOG, 'Uploaded proof of payment.');
                $message = 'Proof of payment uploaded succesfully. Please wait while we review your payment.';
                $message_type = SUCCESS_MESSAGE;
			}
			else
			{
                $message = $image_upload_status['message'];
                $message_type = ERROR_MESSAGE;
			}

		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('deposit');
		
	}


}