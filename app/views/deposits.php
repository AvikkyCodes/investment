<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'User Deposits';
    require_once 'resources/header.php'; 
    require_once 'resources/table_style.php'; 
  ?>

  <style type="text/css">

    #tab {
      padding: 10px;
      margin: 10px;
    }

    .nav {
        padding: 10px;
    }

    .nlink {
        padding: 10px;
        margin: 20px;
        margin-top: 0px;
        border-radius: 5px;
        font-size: 25px;
        font-weight: bold;
        border: 1px solid black;
    }

  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		<?=$data['title'] ?>
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                <?php require_once 'resources/form_sub_msg.php'; ?>

                <div style="overflow:auto;" class="mt-1">
                    <div style="float:right;">
                        <div style="display:inline-block;margin-right:10px;">
                            <form action="process_multiple/deposit" onsubmit="return processMultiple(this)" class="form-inline">
                                <input type="hidden" name="type" value="approve">
                                <input type="hidden" name="reason" value="">
                                <button type="submit" id="multiple1" class="btn btn-success" disabled>Approve Multiple</button>
                            </form>
                        </div>

                        <div style="display:inline-block">
                            <form action="process_multiple/deposit" onsubmit="return processMultiple(this)">
                                <input type="hidden" name="type" value="decline">
                                <textarea name="reason" id="reason" class="form-control mb-1" cols="10" rows="1" required disabled placeholder="reason"></textarea>
                                <button type="submit" id="multiple2" class="btn btn-danger" disabled>Decline Multiple</button>
                            </form>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                
                        <thead>
                        <tr id="mytable">
                            <th id="mytable">Select</th>
                            <th id="mytable">User</th>
                            <th id="mytable">Amount ($)</th>
                            <th id="mytable">Payment Method</th>
                            <th id="mytable">Proof of Payment</th>
                            <th id="mytable">Status</th>
                            <th id="mytable">Date Created</th>
                            <th id="mytable">Approve</th>
                            <th id="mytable">Decline</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php 
                            foreach ($data['deposits'] as $deposit):
                        ?>
                            <tr id="mytable">
                                <td id="mytable">
                                    <input type="checkbox" 
                                        class="form-check-input" 
                                        value="<?=$deposit->id.','.$deposit->amount?>"
                                        name="deposit"
                                    >
                                </td>

                                <td id="mytable">
                                    <?=User::getEmail($deposit->user_id)?>
                                </td>

                                <td id="mytable">
                                    <?=number_format($deposit->amount)?>
                                </td>

                                <td id="mytable">
                                    <?=strtoupper($deposit->payment_method)?>
                                </td>

                                <td id="mytable">
                                    <a href="<?=$deposit->proof?>" target="blank">
                                        <img src="<?=$deposit->proof?>" alt="" width="50" height="50">
                                    </a>
                                </td>

                                <td id="mytable" class="<?=transactionStatStyle($deposit->status)?>">
                                    <?=transactionStatus($deposit->status)?>
                                </td>

                                <td id="mytable">
                                    <?=$deposit->date_created?>
                                </td>

                                <td id="mytable">
                                    <form method="POST" action="process-deposit" class="form-horizontal">
                                        <input type="hidden" name="id" value="<?=$deposit->id?>">
                                        <input type="hidden" name="user_id" value="<?=$deposit->user_id?>">
                                        <input type="hidden" name="action" value="approve">
                                        <input type="hidden" name="reason" value="">
                                        <input type="hidden" name="amount" value="<?=$deposit->amount?>">
                                        <button class="btn btn-success" type="submit"
                                            <?=$deposit->status == 1 ? 'disabled' : ''?>
                                        >
                                            Approve
                                        </button>
                                    </form>
                                </td>

                                <td id="mytable">
                                    <form method="POST" action="process-deposit" class="form-horizontal">
                                        <input type="hidden" name="id" value="<?=$deposit->id?>">
                                        <input type="hidden" name="user_id" value="<?=$deposit->user_id?>">
                                        <input type="hidden" name="action" value="decline">
                                        <textarea name="reason" class="form-control" cols="10" rows="1" required placeholder="reason"
                                        <?=$deposit->status == 2 ? 'disabled' : ''?>></textarea>
                                        <input type="hidden" name="amount" value="<?=$deposit->amount?>">
                                        <button class="btn btn-danger mt-1" type="submit"
                                        <?=$deposit->status == 2 ? 'disabled' : ''?>
                                        >
                                            Decline
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                  
                <script type="text/javascript">
                        // Select all checkboxes with the name 'settings' using querySelectorAll.
                        var checkboxes = document.querySelectorAll("input[type=checkbox][name=deposit]");
                        let selectedDeposits = []

                        /*
                        For IE11 support, replace arrow functions with normal functions and
                        use a polyfill for Array.forEach:
                        https://vanillajstoolkit.com/polyfills/arrayforeach/
                        */

                        // Use Array.forEach to add an event listener to each checkbox.
                        checkboxes.forEach(function(checkbox) {
                        checkbox.addEventListener('change', function() {
                            selectedDeposits = 
                            Array.from(checkboxes) // Convert checkboxes to an array to use filter and map.
                            .filter(i => i.checked) // Use Array.filter to remove unchecked checkboxes.
                            .map(i => i.value) // Use Array.map to extract only the checkbox values from the array of objects.
                            
                            if (selectedDeposits.length !== 0) {
                                document.getElementById("multiple1").disabled = false
                                document.getElementById("multiple2").disabled = false
                                document.getElementById("reason").disabled = false
                            }
                        })
                        });

                        function processMultiple(form) 
                        {
                            var checkboxes = document.querySelectorAll("input[type=checkbox][name=deposit]");
                            let selectedDeposits = []

                            checkboxes.forEach(function(checkbox) {
                                selectedDeposits = 
                                Array.from(checkboxes) // Convert checkboxes to an array to use filter and map.
                                .filter(i => i.checked) // Use Array.filter to remove unchecked checkboxes.
                                .map(i => i.value) // Use Array.map to extract only the checkbox values from the array of objects.
                            })

                            //create data object with form elements
                            let data = {
                                "action": form.type.value,
                                "selected": selectedDeposits,
                                "reason": form.reason.value
                            };

                            //convert data object into JSON format
                            jsonData = JSON.stringify(data);
                            //save data in cookie
                            document.cookie = "multiple=" + jsonData;
                            //redirect to form's action page
                            window.location.href = form.getAttribute("action");
                            
                            //prevent form from submitting
                            return false;

                        }
                    </script>
                  
                </div>
            </div>
          </div>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>