<?php

Class Update_User extends Controller
{

	public function index($action="", $id="")
	{
        if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
        
		$message = ''; $message_type = SUCCESS_MESSAGE;

		try 
		{
			if ($action == 'activate') 
            {
                User::activateUserAccount($id);
                $message = 'User account has been activated.';
                registerLog(ACTIVITY_LOG, 'User account ('. User::getEmail($id) .') activated.');
            } 
            else 
            {
                User::disableUserAccount($id);
                $message = 'User account has been disabled.';
                registerLog(ACTIVITY_LOG, 'User account ('. User::getEmail($id) .') disabled.');
            }
            
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo('users');
	}
	
} 

