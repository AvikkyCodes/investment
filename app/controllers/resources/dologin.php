<?php
//set all required @$_SESSION variables for logged in user

$_SESSION['id'] = $id;

//set user's username for authentication purposes
$_SESSION['username'] = $username;

$_SESSION['full_name'] = $full_name;

$_SESSION['email'] = $email;

$_SESSION['role'] = $role;

$_SESSION['account_balance'] = $account_balance;

//@last_active variable keeps track of session validity
$_SESSION['last_active'] = time();

//keep track of user's logins
$_SESSION['last_logged_in'] = $last_logged_in;

$_SESSION['otp_sent'] = false;

$_SESSION['last_otp'] = time();