<?php

class UserDeposit extends Model
{
	protected static function makePayment($amount, $payment_method, $proof)
	{
		UserDeposit::create([
			'user_id' 					=> getUserId(),
			'amount' 					=> $amount,
			'payment_method' 			=> $payment_method,
			'proof' 					=> $proof,
			'date_created'				=> logTimestamp(),
			'remarks'					=> 'Processing...'
		]);
	}

	protected static function getUserDeposits($id)
	{
		$data = UserDeposit::where('user_id', '=', $id)->orderBy('id', 'desc')->get();
		return $data;
	}

	protected static function hasMadeDeposit($id)
	{
		return UserDeposit::where('user_id', '=', $id)->get()->count() > 1 ? true : false;
	}

	protected static function getApprovedUserDeposits($id)
	{
		$data = UserDeposit::where('status', '=', 1)
		->where('user_id', '=', $id)->get();
		return $data;
	}

	protected static function getApprovedDeposits()
	{
		$data = UserDeposit::where('status', '=', 1)->get();
		return $data;
	}

	protected static function getPendingDeposits()
	{
		$data = UserDeposit::where('status', '=', 0)->get();
		return $data;
	}

	protected static function approveDeposit($id)
	{
		UserDeposit::where('id', '=', $id)->update(['status' => 1, 'remarks' => 'Payment approved. Account credited.']);
	}
	

	protected static function declineDeposit($id, $reason)
	{
		UserDeposit::where('id', '=', $id)->update(['status' => 2, 'remarks' => $reason]);
	}


	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}