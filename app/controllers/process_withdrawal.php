<?php

Class Process_Withdrawal extends Controller
{

	public function index($id="", $user_id="", $action="", $reason="", $amount="")
	{
        if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
        
		$message = ''; $message_type = SUCCESS_MESSAGE;
        $balance = '';
        $withdrawal = UserWithdrawal::find($id);

		try 
		{
			if ($action == 'approve') 
            {
                UserWithdrawal::approveWithdrawal($id);
                UserAccount::updateAccountBalance('debit', $user_id, $amount);
                RecentTransaction::makeTransaction('withdrawal', User::getFullName($user_id) , $amount);
                $message = 'Withdrawal approved successfully.';
                registerLog(ACTIVITY_LOG, 'Withdrawal of $' .number_format($amount).' from '. User::getEmail($user_id) .' approved.');
            } 
            else 
            {
                UserWithdrawal::declineWithdrawal($id, $reason);
                if ($withdrawal->status !== 0) 
                {
                    UserAccount::updateAccountBalance('credit', $user_id, $amount);
                }
                $message = 'Withdrawal declined successfully.';
                registerLog(ACTIVITY_LOG, 'User ('.User::getEmail($user_id).') withdrawal of $' .number_format($amount).' declined.');
            }
            
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo('user-withdrawals');
	}
	
} 

