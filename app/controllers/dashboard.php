<?php

Class Dashboard extends Controller
{

	public function index()
	{
		$msg = getViewMessage();
		$message = $msg['message'];
		$message_type = $msg['message_type'];
		$user_dashboard = $admin_dashboard = [];


		try 
		{
			$id = getUserId();
			$total_profit = $total_bonus = $referral_bonus = $total_ref = $total_deposit = $total_withdrawal = 0;
			$investments = UserInvestment::getClosedUserInvestments($id);
			$bonus = UserTransaction::getUserBonus($id);
			$referral_bonus = UserTransaction::getUserReferralBonus($id);
			$deposits = UserDeposit::getApprovedUserDeposits($id);
			$withdrawals = UserWithdrawal::getApprovedUserWithdrawals($id);

			foreach ($investments as $investment) 
			{ $total_profit += (int) $investment->profit; }

			foreach ($bonus as $bon) 
			{ $total_bonus += (int) $bon->amount; }
			
			foreach ($referral_bonus as $ref_bon) 
			{ $total_ref += (int) $ref_bon->amount; }

			foreach ($deposits as $deposit) 
			{ $total_deposit += (int) $deposit->amount; }
			
			foreach ($withdrawals as $withdrawal) 
			{ $total_withdrawal += (int) $withdrawal->amount; }

			$user_dashboard = 
				[
					'account_balance' => UserAccount::getAccountBalance($id),
			  		'total_investments' => UserInvestment::getUserInvestments($id)->count(),
			  		'open_investments' => UserInvestment::getOpenUserInvestments($id)->count(),
			  		'total_deposit' => $total_deposit,
			  		'total_profit' => $total_profit,
			  		'total_bonus' => $total_bonus,
			  		'referral_bonus' => $total_ref,
			  		'total_withdrawals' => $total_withdrawal,
				];

			$user_deposits = 0;
			$deposits = UserDeposit::getApprovedDeposits();
			foreach ($deposits as $dep) 
			{ $user_deposits += (int) $dep->amount; }

			$user_withdrawals = 0;
			$withdrawals = UserWithdrawal::getApprovedWithdrawals();
			foreach ($withdrawals as $withdrawal) 
			{ $user_withdrawals += (int) $withdrawal->amount; }

			$admin_dashboard = 
				[
					'active_users' => User::active()->count(),
					'total_deposits' => $user_deposits,
					'total_withdrawals' => $user_withdrawals,
					'pending_deposits' => UserDeposit::getPendingDeposits()->count(),
					'pending_withdrawals' => UserWithdrawal::getPendingWithdrawals()->count(),
			  		'total_investments' => UserInvestment::all()->count(),
			  		'logs' => Log::activity()->count()
				];
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDataBaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		//display the dashboard
		$this->view("dashboard", 
			[
				'user_dashboard' => $user_dashboard,
				'admin_dashboard' => $admin_dashboard,
				'message' 			=> $message,
				'message_type' 		=> $message_type
			]
		);
		
	}


}