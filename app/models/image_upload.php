<?php

Class ImageUpload extends Model
{
	
	//function to handle image uploads
  	protected static function uploadImage($dir)
  	{
		$target_dir = ROOT .DS. 'img' .DS. $dir . DS;
    	$target_file = $target_dir . basename($_FILES['image']['name']);
		$target_img = THIS_SERVER . '/' . 'img/' . $dir . '/' . basename($_FILES['image']['name']);
		$uploadOk = 1;
	    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	    $fileSize = $_FILES["image"]["size"];
	    //image upload status
	    $image_upload_status = ['message' => 'okay', 'image' => $target_file];
	    $allowed_formats = ['jpg', 'JPG', 'JPEG', 'jpeg', 'PNG', 'png', 'GIF', 'gif'];
    	// Check if image file is a actual image or fake image
	    $check_image = getimagesize($_FILES["image"]["tmp_name"]);
	    if($check_image !== false) 
	    {
	        $uploadOk = 1;
	    } 
	    else 
	    {
	        $image_upload_status = ['message' => 'File is not an image.'];
	        $uploadOk = 0;
	    }
	    // Check if file already exists
	    if (file_exists($target_file)) 
	    {
	        $image_upload_status = ['message' => 'Sorry, image already exists.'];
	        $uploadOk = 0;
	    }
	    // Check file size
	    if ($fileSize > 10000000) //10MB
	    {
	        $image_upload_status =  ['message' => 'Sorry, your file is too large.'];
	        $uploadOk = 0;
	    }
	    // Allow certain file formats
	    if (!in_array($imageFileType, $allowed_formats)) 
	    {
	        $image_upload_status = ['message' => 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.'];
	        $uploadOk = 0;
	    }
	    // Check if $uploadOk is set to 1
	    if ($uploadOk == 1) 
	    {
	      // if everything is ok, try to upload file
	      if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) 
	      { 
	        $image_upload_status = ['message' => 'okay', 'image' => $target_img];

	        //record uploaded file
	        ImageUpload::create([
				'user_id' => getUserId(),
	        	'image' => $target_file,
	        	'type' => $imageFileType,
	        	'size' => $fileSize,
	        	'location' => $target_dir,
	        	'date_uploaded'	=> logTimestamp()
	        ]);
	      }
	      else{
	        $image_upload_status = ['message' => 'Image upload failed. Try again later.'];
	      }
	    }

    	return $image_upload_status;
  	}



	//relationships
	


}


?>