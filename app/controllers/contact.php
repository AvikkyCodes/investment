<?php

    class Contact extends Controller
    {

        public function index()
        {  
            if (is_logged_in()) 
            {
                redirectTo('dashboard');
            }
            else
            {
                $this->view("contact", 
                    [
                        'message' => '',
                        'message_type' => ''
                    ]
                );
            }
        }
        

    }

?>