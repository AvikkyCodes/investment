<?php

class RecentTransaction extends Model
{
	protected static function makeTransaction($type, $name, $amount)
	{
		RecentTransaction::create([
			'type' 					    => $type,
			'name' 					    => $name,
			'amount' 					=> $amount,
		]);
	}

	protected static function getRecentDeposits()
	{
		$data = RecentTransaction::where('type', '=', 'deposit')->orderBy('id', 'desc')->limit(8)->get();
		return $data;
	}
	
	protected static function getRecentWithdrawals()
	{
		$data = RecentTransaction::where('type', '=', 'withdrawal')->orderBy('id', 'desc')->limit(8)->get();
		return $data;
	}

	


}