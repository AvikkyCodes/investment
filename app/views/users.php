<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Users';
    require_once 'resources/header.php'; 
    require_once 'resources/table_style.php'; 
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		All Users
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                <?php require_once 'resources/form_sub_msg.php'; ?>

                <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                            
                                    <thead>
                                    <tr id="mytable">
                                        <th id="mytable">Full Name</th>
                                        <th id="mytable">User Name</th>
                                        <th id="mytable">Email</th>
                                        <th id="mytable">Phone Number</th>
                                        <th id="mytable">Country</th>
                                        <th id="mytable">Status</th>
                                        <th id="mytable">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php 
                                        foreach ($data['users'] as $user) {
                                            if ($user->id !== getUserId() && $user->role == 2) {
                                    ?>
                                        <tr id="mytable">
                                            <td id="mytable">
                                                <?=$user->full_name?>
                                            </td>

                                            <td id="mytable">
                                                <?=$user->username?>
                                            </td>

                                            <td id="mytable">
                                                <?=$user->email?>
                                            </td>

                                            <td id="mytable">
                                                <?=$user->phone?>
                                            </td>

                                            <td id="mytable">
                                                <?=$user->country?>
                                            </td>

                                            <td id="mytable" class="<?=$user->active ? 'text-success' : 'text-danger'; ?>">
                                                <?=$user->active ? 'Active' : 'Disabled'; ?>
                                            </td>

                                            <td id="mytable">
                                                <form method="POST" action="update-user" class="form-horizontal">
                                                    <input type="hidden" name="action" value="<?=$user->active ? 'disable' : 'activate'; ?>">
                                                    <input type="hidden" name="id" value="<?=$user->id?>">
                                                    <button class="btn btn-sm mt-1 <?=$user->active ? 'btn-danger' : 'btn-success'; ?>" type="submit">
                                                        <?=$user->active ? 'Disable' : 'Activate'; ?>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                    
                  
                </div>
            </div>
          </div>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>