<?php

    class Faq extends Controller
    {

        public function index()
        {  
            if (is_logged_in()) 
            {
                redirectTo('dashboard');
            }
            else
            {
                $this->view("faq", 
                    [
                        'message' => '',
                        'message_type' => ''
                    ]
                );
            }
        }
        

    }

?>