<?php

Class Simulate extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		
        $msg = getViewMessage();

		$this->view('simulate',
					[
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

