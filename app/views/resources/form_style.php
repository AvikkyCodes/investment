<style>

form {
  
}

#regForm {
  background-color: #ffffff;
  width: 100%;
  min-width: 300px;
}


#form-div {
  padding: 20px;
  margin-left: 20px;
}

#step-div {
  text-align: center;
  margin-bottom: 20px;
}

textarea {
  width: 100%;
  height: 200px;
  max-height: 200px;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 80px;
  width: 80px;
  margin: 0 20px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
  float: center;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

.form-sub-heading {
  margin-bottom: 40px;
}

.edit-btn {
  border: 2px solid green;
  border-radius: 5px;
  background-color: green;
  color: white;
  padding: 10px;
}

</style>