<?php

Class Email_Verification extends Controller
{

	public function index()
	{
		if (checkViewData('verification')) 
		{
			$msg = getViewMessage();
			$data = getViewData('verification');
			$email = $data->email;
			
			$this->view('email_verification',
					[
						'email'	=> $email,
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
		} 
		else 
		{
			redirectTo('');
		}
		
	}
	
} 

