<?php

Class Send_Message extends Controller
{

	public function index($message="")
	{
        
		$mssg = 'Message sent successfully. We will get back to you shortly.'; 
        $message_type = SUCCESS_MESSAGE;

		try 
		{
            $mail_status = SendMail::sendEmail(
				SUPPORT_EMAIL, 
				'HELP/SUPPORT', 
				'Message from : ' . getUserEmail() . '<br>' . $message
			);
			
			if (!$mail_status['status']) 
			{
				$mssg = $mail_status['message']; 
        		$message_type = ERROR_MESSAGE;
			}
		}
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$mssg = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($mssg, $message_type);
        redirectTo('support');
	}
	
} 

