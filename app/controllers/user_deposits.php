<?php

Class User_Deposits extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		$msg = getViewMessage();
        $message = $msg['message']; $message_type = $msg['message_type'];
		$deposits = UserDeposit::orderBy('id', 'desc')->get();
        
		$this->view('deposits',
					[
						'title' => 'All User Deposits',
                        'deposits' => $deposits,
						'message' => $message,
						'message_type' => $message_type
					]
				);
	}
	
} 

