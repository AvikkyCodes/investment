<?php

class User extends Model
{
	//retrieve user details for authentication
	protected static function getCredentials($email)
	{
		$user = User::where('email', '=', $email)
				->get();
		return $user;
	}

	//retrieve user details by id
	protected static function getById($id)
	{
		$user = User::find($id);
		return $user;
	}

	protected static function getByEmail($email)
	{
		$user = User::where('email', '=', $email)->first();
		return $user;
	}

	//retrieve user password
	protected static function getPassword($id)
	{
		$user = User::find($id);
		return $user['password'];
	}


	protected static function getRole($id)
	{
		$user = User::find($id);
		return $user['role'];
	}

	protected static function getFullName($id)
	{
		$user = User::find($id);
		return $user['full_name'];
	}

	protected static function getEmail($id)
	{
		$user = User::find($id);
		return $user['email'];
	}

	protected static function getRefId($id)
	{
		$user = User::find($id);
		return $user['referral_id'];
	}

	protected static function getUserId($ref_id)
	{
		$user = User::where('referral_id', '=', $ref_id)->first();
		return $user['id'];
	}

	protected static function getUserReferrals($ref_id)
	{
		$data = User::where('referred_by', '=', $ref_id)->get();
		return $data;
	}

	protected static function updateReferralLevel($id, $level)
	{
		User::where('id', '=', $id)->update(['referral_level' => $level]);
	}

	protected static function updateReferralBonus($id, $bonus)
	{
		User::where('id', '=', $id)->update(['referral_bonus' => $bonus]);
	}


	//change user password
	protected static function changePassword($id, $pass)
	{
		User::where('id', '=', $id)
		->update(
			[
				'password' => encryptPassword($pass)
			]
		);
	}

	protected static function updatePersonalInfo($id, $full_name, $phone, $address, $dob)
	{
		User::where('id', '=', $id)
		->update(
			[
				'full_name' => $full_name,
				'phone' 	=> $phone,
				'address' 	=> $address,
				'dob' 		=> $dob
			]
		);
	}


	//retrieve active users
	protected static function active()
	{
		$user = User::where('active', '=', 1)->where('role', '=', 2)->get();
		return $user;
	}


	//retrieve disabled users
	protected static function disabled()
	{
		$user = User::where('active', '=', 0)->get();
		return $user;
	}

	protected static function hasReferrals($ref_id)
	{
		return User::where('referred_by', '=', $ref_id)->exists() ? true : false;
	}


	//disable user account
	protected static function disableUserAccount($id)
	{
		User::where('id', '=', $id)->update([
			'active' => 0
		]);
	}


	//enable user account
	protected static function activateUserAccount($id)
	{
		User::where('id', '=', $id)->update([
			'active' => 1
		]);
	}	

	protected static function activateUserAccountE($email)
	{
		User::where('email', '=', $email)->update([
			'active' => 1,
			'email_verified' => 1
		]);
	}	
	

	//set last user login time
	protected static function setLastLogin($id, $timestamp)
	{
		User::where('id', '=', $id)
				->update(['last_logged_in' => $timestamp]);
	}

	protected static function userNameExists($username)
	{
		return User::where('username', '=', $username)->exists() ? true : false;
	}

	protected static function userEmailExists($email)
	{
		return User::where('email', '=', $email)->exists() ? true : false;
	}


	protected static function register($username, $name, $email, $phone, $password, $country, $ref_id, $ref_link, $ref_by)
	{
		$user = User::create(
			[
				'username' 	=> $username,
				'full_name' => $name,
				'email' 	=> $email,
				'phone' 	=> $phone,
				'password' 	=> encryptPassword($password),
				'country' 	=> $country,
				'referral_id' => $ref_id,
				'referral_link' => $ref_link,
				'referred_by' => $ref_by,
				'role' 		=> 2,
				'date_registered' => logTimeStamp()
			]
		);

		$user->account()->create(['balance' => 10]);
		$user->transactions()->create([
			'type' => 'bonus',
			'description' => 'Sign Up Bonus',
			'amount' => '10',
			'date_created' => logTimeStamp()
		]);
		$user->settings()->create([
			'bank_name' => '',
			'account_name' => '',
			'account_number' => '',
			'swift_code' => '',
			'btc_address' => '',
			'eth_address' => '',
			'ltc_address' => ''
		]);
		$user->crypto()->create([
			'btc' => 0,
			'eth' => 0,
			'xrp' => 0,
			'ltc' => 0,
			'link' => 0,
			'usdt' => 0,
			'bch' => 0,
			'ada' => 0,
			'bnb' => 0,
		]);
	}


	//relationships

	public function logs() 
	{
		return $this->hasMany(Log::class);
	}

	public function deposits() 
	{
		return $this->hasMany(UserDeposit::class);
	}

	public function withdrawals() 
	{
		return $this->hasMany(UserWithdrawal::class);
	}

	public function account() 
	{
		return $this->hasOne(UserAccount::class);
	}

	public function crypto() 
	{
		return $this->hasOne(UserCrypto::class);
	}

	public function investments() 
	{
		return $this->hasMany(UserInvestment::class);
	}

	public function notifications() 
	{
		return $this->hasMany(Notification::class);
	}

	public function transactions() 
	{
		return $this->hasMany(UserTransaction::class);
	}

	public function settings() 
	{
		return $this->hasOne(UserSetting::class);
	}


}


?>