<?php

Class Do_Withdraw extends Controller 
{

	public function index($amount = "", $mode="", $otp = "")
	{
        $message = $message_type = '';
        $email = getUserEmail();
		$settings = UserSetting::getUserSettings(getUserId());

		try
		{
            $email_verification = EmailVerification::getByEmail($email);

			$q = strtotime($email_verification->expire);

			if (strtotime(date("F d, Y")) <= strtotime(date("F d, Y", $q))) 
			{
				if (decryptPassword($otp, $email_verification->verification_code)) 
				{
                    if (!UserAccount::hasInsufficientFunds(getUserId(), $amount)) 
                    {
						if (!empty($settings->$mode.'_address')) 
						{
							UserWithdrawal::makeWithdrawal($amount, $mode);
							$message = 'Withdrawal request successful. Please wait while it is being processed.';
							$message_type = SUCCESS_MESSAGE;
						} 
						else 
						{
							$message = 'You have not specified a ' .strtoupper($mode). ' address. Update your address in account settings.';
                        	$message_type = ERROR_MESSAGE;
						}
						
                    }
                    else 
                    {
                        $message = 'Sorry, your account balance is too low for this transaction.';
                        $message_type = ERROR_MESSAGE;
                    }
				}
				else
				{
					$message = 'OTP verification failed. Try again.';
					$message_type = ERROR_MESSAGE;
				}
			}
			else
			{
				$message = 'OTP has expired. Request a new one.';
				$message_type = ERROR_MESSAGE;
			}			

		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('withdraw');
		
	}


}