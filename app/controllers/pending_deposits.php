<?php

Class Pending_Deposits extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		$msg = getViewMessage();
        $message = $msg['message']; $message_type = $msg['message_type'];
		$deposits = UserDeposit::getPendingDeposits();
        
		$this->view('deposits',
					[
                        'title' => 'Pending User Deposits',
                        'deposits' => $deposits,
						'message' => $message,
						'message_type' => $message_type
					]
				);
	}
	
} 

