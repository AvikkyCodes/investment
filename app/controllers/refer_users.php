<?php

Class Refer_Users extends Controller
{

	public function index()
	{
		$user = User::find(getUserId());
		$referrals = User::getUserReferrals($user->referral_id);

		$this->view('refer_users',
					[
						'user' => $user,
						'referrals' => $referrals,
						'message' => '',
						'message_type' => ''
					]
				);
	}
	
} 

