<?php

class UserSetting extends Model
{
	protected static function getUserSettings($id)
	{
		$data = UserSetting::where('user_id', '=', $id)->first();
		return $data;
	}

	protected static function getUserWallet($id, $wallet)
	{
		$data = UserSetting::where('user_id', '=', $id)->first();
		return $data[strtolower($wallet).'_address'];
	}

	protected static function updateWithdrawalInfo($id, $bank_name, $account_name, $account_number, 
                                                $swift_code, $btc_address, $eth_address, $ltc_address)
	{
		UserSetting::where('user_id', '=', $id)
		->update(
			[
				'bank_name'         => $bank_name,
				'account_name' 	    => $account_name,
				'account_number' 	=> $account_number,
				'swift_code' 		=> $swift_code,
				'btc_address' 		=> $btc_address,
				'eth_address' 		=> $eth_address,
				'ltc_address' 		=> $ltc_address
			]
		);
	}

    protected static function updateOtherSettings($id, $profit_email, $withdraw_otp, $plan_exp_email)
	{
		User::where('id', '=', $id)
		->update(
			[
				'profit_email'      => $profit_email,
                'withdraw_otp' 	    => $withdraw_otp,
				'plan_exp_email' 	=> $plan_exp_email
			]
		);
	}




	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}