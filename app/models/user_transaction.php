<?php

class UserTransaction extends Model
{
	protected static function makeTransaction($id = '', $type, $description, $amount)
	{
		if ($id == '') 
		{ $id = getUserId(); }
		UserTransaction::create([
			'user_id' 					=> $id,
			'type' 					    => $type,
			'description' 			    => $description,
			'amount' 					=> $amount,
			'date_created'				=> logTimestamp()
		]);
	}

	protected static function getUserTransactions($id)
	{
		$data = UserTransaction::where('user_id', '=', $id)->orderBy('created_at', 'desc')->get();
		return $data;
	}

	protected static function getUserBonus($id)
	{
		$data = UserTransaction::where('user_id', '=', $id)->where('type', '=', 'bonus')->get();
		return $data;
	}

	protected static function getUserReferralBonus($id)
	{
		$data = UserTransaction::where('user_id', '=', $id)->where('type', '=', 'bonus')
		->where('description', '=', 'Referral Bonus')->get();
		return $data;
	}

	protected static function addUserReferralBonus($id, $deposit, $depositor)
	{
		$bonus = 0;
		$user = User::find($id);
		switch ($user['referral_level']) 
		{
			case 1:
				$bonus = (10/100) * $deposit;
				User::updateReferralLevel($id, 2);
				break;
				
			case 2:
				$bonus = (15/100) * $deposit;
				User::updateReferralLevel($id, 3);
				break;

			case 3:
				$bonus = (20/100) * $deposit;
				break;
			
			default:
				$bonus = (10/100) * $deposit;
				break;
		}
		UserTransaction::create([
			'user_id' 					=> $id,
			'type' 					    => 'Bonus',
			'description' 			    => 'Referral Bonus',
			'amount' 					=> $bonus,
			'date_created'				=> logTimestamp()
		]);

		return $bonus;
	}



	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}