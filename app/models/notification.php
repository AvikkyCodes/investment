<?php

class Notification extends Model
{
	
	protected static function notify($user_id, $message)
	{
		Notification::create([
			'user_id' 					=> $user_id,
			'message' 				    => $message,
			'date_created' 				=> logTimestamp()
		]);
	}

	protected static function getUserNotifications($user_id)
	{
		$data = Notification::where('user_id', '=', $id)->where('status', '=', 0)
        ->orderBy('created_at', 'desc')
        ->get();
		return $data;
	}

	protected static function readNotification($id)
	{
		$data = Notification::find($id);
        $data->status = 1;
        $data->save();
		return $data;
	}




	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}