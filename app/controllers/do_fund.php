<?php

Class Do_Fund extends Controller 
{

	public function index($amount = "", $type = "", $user_id = "")
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		
        $message = $message_type = SUCCESS_MESSAGE;

		try
		{
            if ($type == "credit") 
			{
				UserAccount::updateAccountBalance('credit', $user_id, $amount);
				$email = User::getEmail($user_id);
				// Notification::notify($user_id, 'Account funded with $'.number_format($amount).' by Admin.');
				$message = 'Account funded successfully.';
				registerLog(ACTIVITY_LOG, ''.$email.' account funded with $' .number_format($amount).'.');
			} 
			else 
			{
				UserAccount::updateAccountBalance('debit', $user_id, $amount);
				$email = User::getEmail($user_id);
				// Notification::notify($user_id, 'Account defunded of $'.number_format($amount).' by Admin.');
				$message = 'Account defunded successfully.';
				registerLog(ACTIVITY_LOG, ''.$email.' account defunded of $' .number_format($amount).'.');
			}
			
		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('fund-account');
		
	}


}