<?php

Class Do_Register extends Controller
{

	public function index($username="", $name="", $email="", $phone="", $password="",
                            $password_confirmation="", $country="", $ref_by="", $terms="")
	{
		$message = ''; $message_type = ERROR_MESSAGE;
		$redirect = 'register';

		try 
		{
			if (!User::userNameExists($username)) 
			{
				if (!User::userEmailExists($email)) 
				{
					$ref_id = generate_serial_number(6);
					$ref_link = THIS_SERVER . '/' . 'referrals/user/' . $ref_id;
					User::register($username, $name, $email, $phone, $password, $country, $ref_id, $ref_link, $ref_by);
					$user = User::getById($ref_by);

					$mail_status = SendMail::sendEmail($user['email'], 'Dear ' . $user['full_name'] . ' ('.$user['username'].')',
							'You have a new direct signup on henleytradeglobal.com \n' . 
							'User: ' . $username . '\n Name: ' . $name . '\n E-mail: ' . $email . '<br> Thank you.');

					$message = 'Congratulations! you have successfully signed up.';
					$message_type = SUCCESS_MESSAGE;
					setViewData(['email' => $email], 'verification');
					$redirect = 'email-verification';
				} 
				else 
				{
					$user = User::getByEmail($email);
					if (!$user->email_verified) 
					{
						$message = 'User account already exists. Verify your email address to continue.';
						$message_type = ERROR_MESSAGE;
						setViewData(['email' => $email], 'verification');
						$redirect = 'email-verification';
					}
					else
					{
						$message = 'User account with this email already exists.';
					}
				}
			} 
			else 
			{
				$message = 'Username has already been taken. Try another one.';
			}
			
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}

		setViewMessage($message, $message_type);
		redirectTo($redirect);
	}
	
} 

