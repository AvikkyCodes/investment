<?php

Class I_Details extends Controller
{

	public function index($serial="")
	{
        $investment = UserInvestment::getBySerialNumber($serial);

        $q = strtotime($investment->end_date);

        if (strtotime(date("F d, Y")) >= strtotime(date("F d, Y", $q))) 
        {
            if ($investment->status === 'open') 
            {
                UserInvestment::closeInvestment($investment->id);
                $bal = $investment->amount + $investment->profit;
                UserAccount::updateAccountBalance('credit', $investment->user_id, $bal);
                UserTransaction::makeTransaction($investment->user_id, 'investment',
                 'Investment of $'.number_format($investment->amount).' closed with $'.number_format($investment->profit).' profit. Account credited.', $bal);
            }
        }

		setViewData(['serial' => $serial ], 'investment');

        redirectTo('investment-details');
	}
	
} 

