<?php 
	if(!empty($data['message']) && $data['message'] !== '') :
?> 
		<div class="<?=$data["message_type"];?> text-center" >
			<button type="button" class="close" data-dismiss="alert">
				&times;
			</button>
			<strong><?=$data["message"]?></strong>
		</div>

<?php endif; ?>