<?php

Class Process_Deposit extends Controller
{

	public function index($id="", $user_id="", $action="", $reason="", $amount="")
	{
        if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
        
		$message = ''; $message_type = SUCCESS_MESSAGE;
        $balance = '';
        $deposit = UserDeposit::find($id);

		try 
		{
			if ($action == 'approve') 
            {
                UserDeposit::approveDeposit($id);
                UserAccount::updateAccountBalance('credit', $user_id, $amount);
                RecentTransaction::makeTransaction('deposit', User::getFullName($user_id) , $amount);
                $user = User::getById($user_id);
                if (isset($user['referred_by']) && !empty($user['referred_by'])) 
                {
                    if (!UserDeposit::hasMadeDeposit($user_id)) 
                    {
                        $ref = User::getById($user['referred_by']);
                        $ref_id = $ref['id'];
                        $bonus = UserTransaction::addUserReferralBonus($ref_id, $amount, $user_id);
                        UserAccount::updateAccountBalance('credit', $ref_id, $bonus);
                        User::updateReferralBonus($user_id, $bonus);

                        $mail_status = SendMail::sendEmail($user['email'], 'Dear ' . $user['full_name'] . ' ('.$user['username'].')',
							'Your deposit of ' . number_format($amount) . ' has been approved.' . '<br> Thank you.');

                        $mail_status = SendMail::sendEmail($ref['email'], 'Dear ' . $ref['full_name'] . ' ('.$ref['username'].')',
							'You have received a referral comission of $' . number_format($bonus) . ' from the ' .
                            $user['full_name'] . ' ('.$user['username'].') deposit.'  . '<br> Thank you.');
                    }
                }
                $message = 'Deposit approved successfully.';
                registerLog(ACTIVITY_LOG, 'Deposit of $' .number_format($amount).' from '. User::getEmail($user_id) .' approved.');
            } 
            else 
            {
                UserDeposit::declineDeposit($id, $reason);
                if ($deposit->status !== 0) 
                {
                    UserAccount::updateAccountBalance('debit', $user_id, $amount);
                }
                $user = User::getById($user_id);
                $mail_status = SendMail::sendEmail($user['email'], 'Dear ' . $user['full_name'] . ' ('.$user['username'].')',
							'Your deposit of ' . number_format($amount) . ' has been declined.' . '<br> Thank you.');
                $message = 'Deposit declined successfully.';
                registerLog(ACTIVITY_LOG, 'User ('.User::getEmail($user_id).') deposit of $' .number_format($amount).' declined.');
            }
            
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo('user-deposits');
	}
	
} 

