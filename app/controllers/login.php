<?php
// use Illuminate\Database\Capsule\Manager;

Class Login extends Controller
{

    public function index()
    {
        if (is_logged_in()) 
        {
            redirectTo('dashboard');
        }
        else
        {
            $msg = getViewMessage();
                
            $this->view('login',
                [
                    'message' 		=> $msg['message'],
                    'message_type'	=> $msg['message_type']
                ]
            );   
        }   

        
        //  Manager::schema()->create('permissions', 
        //     function($table)
        //     {
        //         $table->integer('id')->unsigned();
        //         $table->integer('name')->unsigned();
        //         $table->timestamps();
               
        //     }
        // );  
        
        // Manager::schema()->create('role_permissions', 
        //     function($table)
        //     {
        //         $table->integer('role_id')->unsigned();
        //         $table->integer('permission_id')->unsigned();
        //         $table->timestamps();
               
        //     }
        // );  
            
    }

}