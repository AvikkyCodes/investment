<?php
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;


Class Model extends BaseModel
{
	use SoftDeletes;

	protected $guarded = [];

}

?>