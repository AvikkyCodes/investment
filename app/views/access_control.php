<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Access Control';
    require_once 'resources/header.php'; 
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- 404 Error Text -->
          <div class="text-center">
          <div class="alert alert-danger">
            <span>
              <i class="fas fa-times-circle fa-lg"></i>
              <h1>ACCESS DENIED!</h1>
            </span>
            <h3>
              You are not authorized to access this resource.
              <br>
              Please contact support.
            </h3>
          </div>
          <div class="">
            <h3><a href="dashboard">&larr; Back to Dashboard</a></h3>
          </div>
        </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
        <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>
