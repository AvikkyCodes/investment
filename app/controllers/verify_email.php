<?php

Class Verify_Email extends Controller 
{

	public function index($email = "", $verification_code = "")
	{
        $message = $message_type = '';
		$redirect = 'login';

		try
		{
            $email_verification = EmailVerification::getByEmail($email);
			$q = strtotime($email_verification->expire);
			$user = User::getByEmail($email);

			if (!$user->email_verified) 
			{
				if (strtotime(date("F d, Y")) <= strtotime(date("F d, Y", $q))) 
				{
					if (decryptPassword($verification_code, $email_verification->verification_code)) 
					{
						User::activateUserAccountE($email);
						$mail_status = SendMail::sendEmail($user['email'], 'Dear ' . $user['full_name'] . ' ('.$user['username'].')',
							'You have just received a $10 signup bonus.' . '<br> Thank you.');
						$message = 'Email has been verified successfully. You can now login.';
						$message_type = SUCCESS_MESSAGE;
					}
					else
					{
						$message = 'Email verification failed. Try again.';
						$message_type = ERROR_MESSAGE;
						$redirect = 'email-verification';
					}
				}
				else
				{
					$message = 'Email verification link has expired. Request a new link.';
					$message_type = ERROR_MESSAGE;
					$redirect = 'email-verification';
				}				
			} 
			else 
			{
				$message = 'Email has already been verified. Login to continue.';
				$message_type = ERROR_MESSAGE;
			}
			


		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo($redirect);
		
	}


}