<div class="tab">

  <div class="form-sub-heading form-group">
    <h3 class="text-center">
      Review and Submit
    </h3>
  </div>

  <div class="form-group">
    <div class="text-center alert alert-info">
      <p>
        USE THE NEXT AND PREVIOUS BUTTONS TO 
        NAVIGATE THROUGH THE FORM PAGES.
        <br>
        PLEASE THOROUGHLY REVIEW ALL INPUTED DATA BEFORE SUBMITING THE FORM.
      </p>
    </div>
  </div>
  
</div>


<div style="overflow:auto;" class="mt-1">
  <div style="float:right;">
    <button type="button" id="prevBtn" onclick="nextPrev(-1)" class="btn btn-primary mr-1">
      Previous
    </button>
    <button type="button" id="nextBtn" onclick="nextPrev(1)" class="btn btn-success"> 
      Next
    </button>
    <input type="submit" id="submitBtn" class="btn btn-success" value="<?=$submit_button ?>">
  </div> 
</div>
