<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from orchardcapitaltraders.com/about by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2022 09:02:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="LlJDpqNPeWhT92XG7PKkbav1uBTHsSMPoS3a1U2g">
        <title>Henley Trade Global | About Us</title>
        
        <meta name="description"
            content="We offers Crypto Assets Management of the highest quality on the basis of foreign exchange and profitable trade through Bitcoin exchanges. There is no other worldwide financial market that can guarantee a daily ability to generate constant profit with the large price swings of Bitcoin.">
        
        <!-- Google / Search Engine Tags -->
        <meta itemprop="name" content="OCTraders - Welcome to Orchard Capital Traders">
        <meta itemprop="description"
            content="We offers Crypto Assets Management of the highest quality on the basis of foreign exchange and profitable trade through Bitcoin exchanges. There is no other worldwide financial market that can guarantee a daily ability to generate constant profit with the large price swings of Bitcoin.">
        <meta itemprop="image" content="temp/images/meta.html">
        
        <link rel="icon" href="storage/app/public/photos/rCyrNPorchardcapitaltraders_logo.html" type="image/png"/>
                    
            <link href="temp/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- Icons -->
            <link href="temp/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        
            <!-- Magnific -->
            <link rel="stylesheet" href="temp/css/line.css">
            <link href="temp/css/flexslider.css" rel="stylesheet" type="text/css" />
            <link href="temp/css/magnific-popup.css" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css')}}" />
        
            <!-- Slider -->
            <link rel="stylesheet" href="temp/css/owl.carousel.min.css" />
            <link rel="stylesheet" href="temp/css/owl.theme.default.min.css" />
            <!-- Main Css -->
            <link href="temp/css/style.css" rel="stylesheet" type="text/css" />
            <link href="temp/css/colors/default.css" rel="stylesheet">
        

    </head>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/6323372537898912e96960d6/1gd0ourl8';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    <body>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        {tawk to codess}
        </script>
       
        
    <!-- Navbar STart -->
    <header id="topnav" class="sticky defaultscroll bg-white">
        <div class="container">
            <!-- Logo container-->
            <div>
                <a class="logo" href="/">
                    <img src="images/Henley Trade.jpg" height="35" alt="" class="mr-2">
                </a>
            </div>
            <div class="buy-button">

                <a href="login" class="mr-3 btn btn-warning login-btn-success">Login</a>
                <a href="register" class="btn btn-warning login-btn-success ">Get Started</a>


            </div>
            <!--end login button-->
            <!-- End Logo container-->
            <div class="menu-extras">
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li><a href="/" class="text-dark">Home</a></li>
                    <li><a href="about" class="text-dark">About us</a></li>
                   <!-- <li><a href="#pricing" class="text-dark">Pricing</a></li>-->
                    <li><a href="faq" class="text-dark">FAQ</a></li>
                    <li><a href="contact" class="text-dark">Contact</a></li>

                </ul>


                </ul>
                <!--end navigation menu-->
                <div class="buy-menu-btn d-none">
                    <a href="login" target="_blank" class="btn btn-warning">Login</a>
                    <a href="register" target="_blank" class="btn btn-warning">Get Started</a>
                </div>
                <!--end login button-->
            </div>
            <!--end navigation-->
        </div>
        <!--end container-->
    </header>
    <!--end header-->
    <!-- Navbar End -->
    
    <!-- Hero Start -->
    <section class="bg-half bg-dark d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="text-center col-lg-12">
                    <div class="page-next-level">
                        <h4 class="title text-light">ABOUT US</h4>
                        <div class="page-next">
                            <nav aria-label="breadcrumb" class="d-inline-block">
                                <ul class="mb-0 bg-warning rounded shadow breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Henley Global Traders</a></li>
                                    <li class="breadcrumb-item  text-white" >About Us</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="overflow-hidden text-white shape">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- About Start -->
    <section class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="pt-2 mt-4 col-lg-5 col-md-5 mt-sm-0 pt-sm-0">
                    <div class="position-relative">
                        <img src="storage/app/public/photos/iAwfKeabout.png" class="mx-auto rounded img-fluid d-block"
                            alt="">
                        <div class="play-icon">
                            <!-- <a href="http://vimeo.com/287684225" class="play-btn video-play-icon">
                                <i class="bg-white shadow mdi mdi-play text-primary rounded-circle"></i>
                            </a> -->
                        </div>
                    </div>
                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-lg-7 col-md-7 mt-sm-0 pt-sm-0">
                    <div class="section-title ml-lg-4">
                        <h4 class="mb-4 title">Who we are</h4>
                      <!--  <p class="text-muted">online trade 
                            is a solution for creating an investment management platform. It is suited for
                            hedge or mutual fund managers and also Forex, stocks, bonds and cryptocurrency traders who
                            are looking at runing pool trading system. Onlinetrader simplifies the investment,
                            monitoring and management process. With a secure and compelling mobile-first design,
                            together with a default front-end design, it takes few minutes to setup your own investment
                            management or pool trading platform.</p>-->
                            <p>
Henley Trade Global is a global leader in financial management business, We are not a Start up, we are a team with proven integrity and successful financial track records for over 4 years extending across the global financial markets. Today, the company offers services in more than 40 countries, providing some of the broadest, most sophisticated financial management. </p>
<p>Henley Trade Global continues its pioneering work of transforming thousandaires into millions, with advanced financial management skills and experts using intelligent digital and web solutions, including our flagship website. As we continue to seek more strategic ways to expand our offerings in financial management, we have taken steps to ensure that we have the processes, technology to deliver the highest level of service and support to our customers. In 2019 we became officially incorporated, therefore we adhere to strict KYC and AML global compliance standards. Let your crypto work for you.</p>

                        <a href="login" class="mt-3 btn btn-warning">Invest Now<i
                                class="mdi mdi-chevron-right"></i></a>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>


    <!-- About End -->

    <!-- Team Start -->
    <section class="section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="pb-2 mb-4 text-center section-title">
                        <h6 class="text-warning">Get Started</h6>
                        <h4 class="mb-4 title">How to get started ?</h4>

                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->

            <div class="row">
                <div class="pt-2 mt-4 col-md-4">
                    <div
                        class="text-center bg-transparent border-0 card features feature-clean work-process process-arrow">
                        <div class="mx-auto bg-dark text-center icons rounded text-warning">
                            <i class="mb-0 rounded uil uil-user-check d-block h3"></i>
                        </div>

                        <div class="card-body">
                            <h5 class="text-dark">Create an Account</h5>
                            <p class="mb-0 text-muted">Create an account with us using your prefered email/username</p>
                        </div>
                    </div>
                </div> 
                <!--end col-->

                <div class="pt-2 mt-4 col-md-4 mt-md-5 pt-md-3">
                    <div
                        class="text-center bg-transparent border-0 card features feature-clean work-process process-arrow">
                        <div class="mx-auto bg-dark rounded text-center icons text-warning">
                            <i class="mb-0 rounded uil uil-transaction d-block h3"></i>
                        </div>

                        <div class="card-body">
                            <h5 class="text-dark">Make a Deposit</h5>
                            <p class="mb-0 text-muted">Make A deposit with any of your preffered currency</p>
                        </div>
                    </div>
                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-md-4 mt-md-5 pt-md-5">
                    <div
                        class="text-center bg-transparent border-0 card features feature-clean work-process d-none-arrow">
                        <div class="mx-auto text-center icons text-warning rounded bg-dark">
                            <i class="mb-0 rounded uil uil-analysis d-block h3"></i>
                        </div>

                        <div class="card-body">
                            <h5 class="text-dark">Start Trading/Investing</h5>
                            <p class="mb-0 text-muted">Start trading with  commodities e.tc</p>
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Team End -->

    <!-- Sign up Section Start -->
    <section class="overflow-hidden section mt-60 bg-light">
        <div class="container">
            <div class="p-4 roundedd bg-dark p-lg-5">
                <div class="row align-items-end">
                    <div class="col-md-8">
                        <div class="text-center section-title text-md-left">
                            <h4 class="mb-3 text-white title title-dark">The Better Way to Trade &amp; Invest</h4>
                            <p class="mb-0 text-white">Henley Trade Global helps over 2 million customers achieve their financial goals by helping them trade and invest with ease</p>
                        </div>
                    </div>
                    <!--end col-->

                    <div class="mt-4 col-md-4 mt-sm-0">
                        <div class="text-center text-md-right">
                            <a href="register" class="pt-3 pb-3 pl-4 pr-4 btn btn-light">Create Free
                                Account</a>
                        </div>
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- End Sign up Section  -->

    
    
    
    
    <!-- Footer Start -->
    <footer class="footer bg-dark">
        <div class="container">
            <div class="row">
                <div class="pb-0 mb-0 col-lg-4 col-12 mb-md-4 pb-md-2">
                    <h5 class="text-light footer-head">Henley Trade Global</h5>
                    <p class="mt-4">We offer Crypto Assets Management of the highest quality on the basis of foreign exchange and profitable trade through Bitcoin exchanges. There is no other worldwide financial market that can guarantee a daily ability to generate constant profit with the large price swings of Bitcoin.</p>

                    <ul class="mt-4 mb-0 list-unstyled social-icon social">
                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                    data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                    data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                    data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                    </ul>
                    <!--end icon-->

                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-lg-4 col-md-4 mt-sm-0 pt-sm-0">
                    <h5 class="text-light footer-head">Useful Links</h5>
                    <ul class="mt-4 list-unstyled footer-list">
                        <li><a href="/" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                Home</a></li>
                        <li><a href="about" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                About us</a></li>
                        <li><a href="contact" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                Contact Us</a></li>
                        <li><a href="faq" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                Faq</a></li>
                    </ul>
                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-lg-4 col-md-4 mt-sm-0 pt-sm-0">
                    <h5 class="text-light footer-head">Contact Details</h5>
                    <div class="mt-2">
                        <h6 class="text-foot"><i class="mr-1 mdi mdi-home"> </i>
                            Head Office</h6>
                        <p>50 Grosvenor Hill. W1K 3QY London.</p>
                        <h6><i class="mr-1 mdi mdi-email"> </i>Email Address</h6>
                        <p>support@HenleyTradeGlobal</p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </footer>
    <!--end footer-->
    <footer class="footer footer-bar bg-dark">
        <div class="container text-center">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="text-sm-left">
                        <p class="mb-0">
                            © Copyright 2022 HenleyTradeGlobal All
                            Rights Reserved.
                        </p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </footer>
    <!--end footer-->
    <!-- Footer End -->
    
    

                  <!-- javascript -->
            <script src="temp/js/jquery-3.5.1.min.js"></script>
            <script src="temp/js/bootstrap.bundle.min.js"></script>
            
            <!-- SLIDER -->
            <script src="temp/js/owl.carousel.min.js"></script>
            <script src="temp/js/owl.init.js"></script>
            <!-- Icons -->
            <script src="temp/js/feather.min.js"></script>
            <script src="temp/js/bundle.html"></script>
            
            <script src="temp/js/app.js"></script>
            <script src="temp/js/widget.js"></script>
       

    </body>

<!-- Mirrored from orchardcapitaltraders.com/about by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2022 09:02:07 GMT -->
</html>
