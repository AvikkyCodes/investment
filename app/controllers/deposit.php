<?php

Class Deposit extends Controller
{

	public function index()
	{
		$msg = getViewMessage();

		$this->view('deposit',
					[
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

