<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from orchardcapitaltraders.com/register by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2022 09:01:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="LlJDpqNPeWhT92XG7PKkbav1uBTHsSMPoS3a1U2g">
        <title>Henley Trade Global | Email Verification</title>
        
        
        <link rel="icon" href="storage/app/public/photos/rCyrNPorchardcapitaltraders_logo.html" type="image/png"/>
                   
            <link href="temp/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- Icons -->
            <link href="temp/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        
            <link rel="stylesheet" href="temp/css/line.css">
            <script src="../www.google.com/recaptcha/api.js" async defer></script>
            <!-- Main Css -->
            <link href="temp/css/style.css" rel="stylesheet" type="text/css" />
            <link href="temp/css/colors/default.css" rel="stylesheet">
        

    </head>
    <body class="h-100 bg-dark">
       <section class="y auth">
        <div class="container">
            <div class="pb-3 row justify-content-center">

                
                    
                     <div class="col-12 col-md-6 col-lg-6 col-sm-10 col-xl-6">
                                                    
                    
                    <div class="bg-white shadow card login-page roundedd border-1 ">

                        <div class="card-body">
                            
                    <div class="text-center">
                        <a href="/"><img src="images/Henley Trade.jpg" alt="" class="mb-3 img-fluid auth__logo"></a>
                    </div>
                            <div class="text-center card-title text-success">
                                <?php require_once('resources/form_sub_msg.php'); ?>
                            </div>
                            <p class="text-lg text-dark text-center">For security reasons, we need to verify your email address. <br>
                                We will send you a verification link.
                            </p>
                            <form method="POST" action="send-code" class="mt-4 login-form">
                                 
                                    
                                    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Email Address:</label>
                                            <div class="position-relative">
                                                <i data-feather="mail" class="fea icon-sm icons"></i>
                                                <input type="email" class="pl-5 form-control" name ="email" value="<?=$data['email']?>" id="email" readonly required>
                                            </div>
                                        </div>
                                    </div>                            
                                    <!--end col-->

                                    <div class="mb-0 col-lg-12">
                                        <button class="btn btn-primary btn-block pad" type="submit">Send Verification Link</button>
                                    </div>
                                    <!--end col-->

                                 
                                    
                            </form>
                        </div>
                    </div>
                    <!---->
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->




                 <script src="temp/js/jquery-3.5.1.min.js"></script>
            <script src="temp/js/bootstrap.bundle.min.js"></script>
            
            <!-- SLIDER -->
            <script src="temp/js/owl.carousel.min.js"></script>
            <script src="temp/js/owl.init.js"></script>
            <!-- Icons -->
            <script src="temp/js/feather.min.js"></script>
            <script src="temp/js/bundle.html"></script>
            
            <script src="temp/js/app.js"></script>
            <script src="temp/js/widget.js"></script>
       
<script>
    $('#input1').on('keypress', function(e) {
        return e.which !== 32;
    });
</script>
    </body>

<!-- Mirrored from orchardcapitaltraders.com/register by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2022 09:01:26 GMT -->
</html>
