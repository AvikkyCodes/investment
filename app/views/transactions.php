<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Transaction History';
    require_once 'resources/header.php'; 
    require_once 'resources/table_style.php'; 
  ?>

  <style type="text/css">

    #tab {
      padding: 10px;
      margin: 10px;
    }

    .nav {
        padding: 10px;
    }

    .nlink {
        padding: 10px;
        margin: 20px;
        margin-top: 0px;
        border-radius: 5px;
        font-size: 25px;
        font-weight: bold;
        border: 1px solid black;
    }

  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		Transactions On Your Account
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                    <ul class="nav nav-tabs">
                        <li class="nlink"><a data-toggle="tab" href="#home">DEPOSITS</a></li>
                        <li class="nlink"><a data-toggle="tab" href="#menu1">WITHDRAWALS</a></li>
                        <li class="nlink"><a data-toggle="tab" href="#menu2">OTHERS</a></li>
                    </ul>

                    <div class="tab-content">

                        <!-- DEPOSITS -->
                        <div id="home" class="tab-pane active">
                            <h2 class="text-center mt-2">DEPOSITS</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                            
                                    <thead>
                                    <tr id="mytable">
                                        <th id="mytable">Amount ($)</th>
                                        <th id="mytable">Payment Method</th>
                                        <th id="mytable">Status</th>
                                        <th id="mytable">Date Created</th>
                                        <th id="mytable">Remarks</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php 
                                        foreach ($data['deposits'] as $deposit):
                                    ?>
                                        <tr id="mytable">
                                            <td id="mytable">
                                                <?=number_format($deposit->amount)?>
                                            </td>

                                            <td id="mytable">
                                                <?=strtoupper($deposit->payment_method)?>
                                            </td>

                                            <td id="mytable" class="<?=transactionStatStyle($deposit->status)?>">
                                                <?=transactionStatus($deposit->status)?>
                                            </td>

                                            <td id="mytable">
                                                <?=$deposit->date_created?>
                                            </td>

                                            <td id="mytable">
                                                <?=$deposit->remarks?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <!-- WITHDRAWALS -->
                        <div id="menu1" class="tab-pane fade">
                        <h2 class="text-center mt-2">WITHDRAWALS</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                            
                                    <thead>
                                    <tr id="mytable">
                                        <th id="mytable">Amount ($)</th>
                                        <th id="mytable">Receiving Mode</th>
                                        <th id="mytable">Status</th>
                                        <th id="mytable">Date Created</th>
                                        <th id="mytable">Remarks</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php 
                                        foreach ($data['withdrawals'] as $withdrawal):
                                    ?>
                                        <tr id="mytable">
                                            <td id="mytable">
                                                <?=number_format($withdrawal->amount)?>
                                            </td>

                                            <td id="mytable">
                                                <?=strtoupper($withdrawal->receiving_mode)?>
                                            </td>

                                            <td id="mytable" class="<?=transactionStatStyle($withdrawal->status)?>">
                                                <?=transactionStatus($withdrawal->status)?>
                                            </td>

                                            <td id="mytable">
                                                <?=$withdrawal->date_created?>
                                            </td>

                                            <td id="mytable">
                                                <?=$withdrawal->remarks?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>  
                        </div>


                        <!-- OTHERS -->
                        <div id="menu2" class="tab-pane fade">
                        <h2 class="text-center mt-2">OTHERS</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                            
                                    <thead>
                                    <tr id="mytable">
                                        <th id="mytable">Type</th>
                                        <th id="mytable">Amount ($)</th>
                                        <th id="mytable">Remarks</th>
                                        <th id="mytable">Date Created</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php 
                                        foreach ($data['transactions'] as $transaction):
                                    ?>
                                        <tr id="mytable">
                                            <td id="mytable">
                                                <?=ucfirst($transaction->type)?>
                                            </td>

                                            <td id="mytable">
                                                <?=number_format($transaction->amount)?>
                                            </td>

                                            <td id="mytable">
                                                <?=$transaction->description?>
                                            </td>

                                            <td id="mytable">
                                                <?=$transaction->date_created?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                  
                </div>
            </div>
          </div>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>