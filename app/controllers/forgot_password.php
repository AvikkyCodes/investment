<?php

    class Forgot_Password extends Controller
    {

        public function index()
        {  
            if (is_logged_in()) 
            {
                redirectTo('dashboard');
            }
            else
            {
                $this->view("forgot_password", 
                    [
                        'message' => '',
                        'message_type' => ''
                    ]
                );
            }
        }
        

    }

?>