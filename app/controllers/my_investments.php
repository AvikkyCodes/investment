<?php

Class My_Investments extends Controller
{

	public function index()
	{
		if (UserInvestment::hasInvestment(getUserId())) 
		{
			$msg = getViewMessage();
	
			$investments = UserInvestment::getUserInvestments(getUserId());

			foreach ($investments as $investment) 
			{
				$q = strtotime($investment->end_date);
				
				if (strtotime(date("F d, Y")) >= strtotime(date("F d, Y", $q))) 
				{
					if ($investment->status === 'open') 
					{
						UserInvestment::closeInvestment($investment->id);
						$bal = $investment->amount + $investment->profit;
						UserAccount::updateAccountBalance('credit', $investment->user_id, $bal);
						UserTransaction::makeTransaction($investment->user_id, 'investment',
						'Investment of $'.number_format($investment->amount).' closed with $'.number_format($investment->profit).' profit. Account credited.', $bal);
					}
				}
			}

	
			$this->view('user_investments',
						[
							'investments' => $investments,
							'message' => $msg['message'],
							'message_type' => $msg['message_type']
						]
					);
		}
		else {
			setViewMessage('You do not have any active plans yet. Subscribe to a plan to continue.', ERROR_MESSAGE);
			redirectTo('join-plan');
		}

	}
	
} 

