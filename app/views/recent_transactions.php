<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Recent Transactions';
    require_once 'resources/header.php';
  ?>

</head>

<body id="page-top">

  
  <main class="main" id="top">
    

      <section class="py-xxl-10 pb-0" id="home">
        <div class="container">
          <div class="row align-items-center">
            <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mt-4 mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                        RECENT WITHDRAWAL AND DEPOSIT
                    </h1>
                  </div>
                  <!-- Card Body -->
                <div class="card-body">

                    <div class="row mt-3">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="text-white bg-dark">
                                            <th colspan="2" id="mytable">Recent Deposits</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            foreach ($data['deposits'] as $deposit) {
                                            
                                        ?>
                                        <tr class="text-dark">
                                            <td id="mytable"><?=$deposit->name ?></td>
                                            <td id="mytable">$<?=number_format($deposit->amount) ?></td>
                                        </tr>
                                        <?php
                                            
                                            } 
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="text-white bg-dark">
                                            <th colspan="2" id="mytable">Recent Withdrawals</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            foreach ($data['withdrawals'] as $withdrawal) {
                                            
                                        ?>
                                        <tr class="text-dark">
                                            <td id="mytable"><?=$withdrawal->name ?></td>
                                            <td id="mytable">$<?=number_format($withdrawal->amount) ?></td>
                                        </tr>
                                        <?php
                                            
                                            } 
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
      </section>


  </main>


</body>

</html>
