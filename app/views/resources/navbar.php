<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

  <!-- Sidebar Toggle (Topbar) -->
  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
  </button>

  <div style="display:inline-block;">
    <?php if(is_an_admin()){?>
      <a href="fund-account" class="btn btn-primary btn-sm">Fund Account</a>
      <a href="wallets" class="btn btn-secondary btn-sm ml-2">Wallets</a>
    <?php } else {?>
    <a href="deposit" class="btn btn-primary btn-sm">Fund Account</a>
    <a href="withdraw" class="btn btn-secondary btn-sm ml-2">Withdraw Funds</a>
    <?php } ?>
  </div>

  <div class="topbar-divider d-none d-sm-block"></div>

  <span class="d-none d-lg-inline">
  
    <?php 
      $last_log = getUserLastLogin();

      if (empty($last_log) || is_null($last_log))
      { $last_log = 'Congratulations! on your first login'; } 
    

      // if(!empty($last_log) && !is_null($last_log)) 
      // {}
    ?>


    <!-- Topbar Navbar -->

    <i class="fas fa-clock mr-1 ml-1" tyle="font-size: 12px;"></i>
    Last logged in : <i style="font-size: 12px;"><?=$last_log ?></i>
    
  </span>

  


  <ul class="navbar-nav ml-auto">

    
  <div class="mt-3 ml-2" id="google_translate_element"></div>
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
// ]]></script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
    <!-- Nav Item - Messages -->
    

    <div class="topbar-divider d-none d-sm-block"></div>

    <!-- Nav Item - User Information -->
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer;">
        <?php if(!is_an_admin()): ?>
          <span class="mr-3 text-success text-bolder">$<?=number_format(getUserAcBalance())?></span> 
        <?php endif; ?>
        <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
      </a>
      <!-- Dropdown - User Information -->
      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
        <i class="dropdown-item text-dark"><?=getUserFullName()?></i>
        <?php if(!is_an_admin()) :?>
        <i class="dropdown-item"><?=getUserEmail()?></i>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="deposit">
          Deposit
        </a>
        <a class="dropdown-item" href="withdraw">
          Withdraw
        </a>
        <a class="dropdown-item" href="join-plan">
          Join Plan
        </a>
        <div class="dropdown-divider"></div>
        <?php endif;?>
        <a class="dropdown-item" href="account-settings">
          Account Settings
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal" style="cursor:pointer;">
          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          Logout
        </a>
      </div>
    </li>

  </ul>
  
</nav>

<div class="ml-5 mr-2 mb-2">
  <a  href="<?=$_SERVER['HTTP_REFERER']?>" class="btn btn-primary btn-sm"> &larr; </a>
</div>