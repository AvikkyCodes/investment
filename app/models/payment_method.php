<?php

class PaymentMethod extends Model
{
	//get selected payment method
	protected static function getAddress($method)
	{
		$method = PaymentMethod::where('method', '=', $method)->first();
		return $method['address'];
	}

	protected static function updateInfo($btc_address, $eth_address)
	{
		PaymentMethod::where('method', '=', 'btc')->update(['address' => $btc_address]);
		PaymentMethod::where('method', '=', 'eth')->update(['address' => $eth_address]);
	}




	//relationships


}