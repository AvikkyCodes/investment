<?php if($page_title):?>
<title><?=$page_title ? 'Henley Trade Global | ' . $page_title : '' ?> </title>
<?php endif;?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/x-icon" href="images/Henley Trade.jpg">
<link href="vendor/components/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

<!-- Custom fonts for this template-->
<link href="vendor/components/font-awesome/css/all.min.css" rel="stylesheet" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<link href="css/style.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="vendor/onokumus/metismenu/dist/metisMenu.min.css">

<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<link href="css/sb-admin-2.css" rel="stylesheet" type="text/css">




<style type="text/css">
	a{
	  cursor: pointer;
	}

	a:hover {
	  text-decoration: none;
	}
</style>
