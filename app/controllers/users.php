<?php

Class Users extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		
        $msg = getViewMessage();

        $users = User::orderBy('id', 'desc')->get();

		$this->view('users',
					[
                        'users' => $users,
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

