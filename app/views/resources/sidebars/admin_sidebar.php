      <!-- Heading -->
      <div class="sidebar-heading">
        Main-Functions
      </div>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Nav Item -->
      <li class="nav-item">
          <a class="nav-link" href="users">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
      </li>

      <li class="nav-item">
          <a class="nav-link" href="user-deposits">
            <i class="fas fa-fw fa-download"></i>
            <span>User Deposits</span>
          </a>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link" href="user-investments">
          <i class="fas fa-fw fa-coins"></i>
          <span>User Investments</span>
        </a>
      </li>
      
      <li class="nav-item">
          <a class="nav-link" href="user-withdrawals">
            <i class="fas fa-fw fa-upload"></i>
            <span>User Withdrawals</span>
          </a>
      </li>

      <li class="nav-item">
          <a class="nav-link" href="user-referrals">
            <i class="fas fa-fw fa-retweet"></i>
            <span>User Referrals</span>
          </a>
      </li>
      
      <li class="nav-item">
          <a class="nav-link" href="simulate">
            <i class="fas fa-fw fa-recycle"></i>
            <span>Recent Transactions</span>
          </a>
      </li>


      <!-- <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne"
              aria-expanded="true" aria-controls="collapseOne">
              <i class="fas fa-fw fa-download"></i>
              <span>Deposits</span>
          </a>
          <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
              <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Deposits:</h6>
                  <a class="collapse-item" href="pending-deposits">Pending Deposits</a>
                  <a class="collapse-item" href="user-deposits">User Deposits</a>
                  <a class="collapse-item" href="simulate">Make Deposit</a>
              </div>
          </div>
      </li> -->

      <!-- <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
              aria-expanded="true" aria-controls="collapseTwo">
              <i class="fas fa-fw fa-upload"></i>
              <span>Withdrawals</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
              <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Withdrawals:</h6>
                  <a class="collapse-item" href="user-withdrawals">User Withdrawals</a>
                  <a class="collapse-item" href="simulate">Make Withdrawal</a>
              </div>
          </div>
      </li> -->

      <!-- <li class="nav-item">
          <a class="nav-link" href="wallets">
            <i class="fas fa-fw fa-wallet"></i>
            <span>Wallets</span>
          </a>
      </li> -->
      
      <!-- Nav Item -->
      <li class="nav-item">
          <a class="nav-link" href="logs">
            <i class="fas fa-fw fa-list"></i>
            <span>Activity Logs</span>
          </a>
      </li>


      