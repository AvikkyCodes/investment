<?php

Class Join_Plan extends Controller
{

	public function index()
	{
        $msg = getViewMessage();

		$this->view('investment_plans',
					[
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

