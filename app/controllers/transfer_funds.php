<?php

Class Transfer_Funds extends Controller
{

	public function index()
	{
        $msg = getViewMessage();

		$this->view('fund_transfer',
					[
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

