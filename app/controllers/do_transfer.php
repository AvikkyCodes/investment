<?php

Class Do_Transfer extends Controller 
{

	public function index($email = "", $amount = "")
	{
        $message = $message_type = SUCCESS_MESSAGE;

		try
		{
			if (UserAccount::hasInsufficientFunds(getUserId(), $amount)) 
            {
                $message = 'Sorry, your account balance is too low for this transaction.';
                $message_type = ERROR_MESSAGE;
            }
			else 
			{
				$user = User::getByEmail($email);
				UserAccount::updateAccountBalance('credit', $user['id'], $amount);
				UserAccount::updateAccountBalance('debit', getUserId(), $amount);
				UserTransaction::makeTransaction(getUserId(), 'transfer', 'Funds transferred to '.$email, $amount);
				UserTransaction::makeTransaction($user['id'], 'transfer', 'Funds received from '.getUserEmail(), $amount);
				// Notification::notify($user['id'], 'Transfer of $'.number_format($amount).' received from '.getUserEmail().'.');
				$message = 'Fund transfer successfull.';
				registerLog(ACTIVITY_LOG, ''.getUserEmail().' 
				transferred $' .number_format($amount).' to '.$user->email.'.');
			} 
		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('transfer-funds');
		
	}


}