<?php

Class Logs extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		
		$logs = Log::orderBy('id', 'desc')->get();

		$this->view('logs',
			[
				'logs' => $logs,
				'message' => '',
				'message_type' => ''
			]
		);
	}
}

?>