<?php

class UserCrypto extends Model
{
	protected static function getUserCrypto($id)
	{
		$data = UserCrypto::where('user_id', '=', $id)->first();
		return $data;
	}

	protected static function getCryptoBalance($id, $crypto)
	{
		$data = UserCrypto::where('user_id', '=', $id)->first();
		return $data[$crypto];
	}

    protected static function hasInsufficientFunds($id, $crypto, $amount)
	{
		$balance = UserCrypto::getCryptoBalance($id, $crypto);
		return ($balance < $amount) ? true : false;
	}

	protected static function updateUserCrypto($action, $id, $crypto, $amount)
	{
        $balance = UserCrypto::getCryptoBalance($id, $crypto);
        $newBalance = 0.0;
		$bal = (float) $balance;
		$amt = (float) $amount;
		$action == 'credit' ? $newBalance = $bal + $amt : $newBalance = $bal - $amt;
		UserCrypto::where('user_id', '=', $id)->update([$crypto  => $newBalance]);
	}

    




	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}