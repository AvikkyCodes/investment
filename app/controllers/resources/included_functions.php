<?php

/*
**Custom Helper functions
*/

  //redirection function
  function redirectTo($route)
  {
    header("Location: " . THIS_SERVER . DS . $route);
  }

  //convert to url friendly
  function toUrlFriendly($url)
  {
    $url = str_replace('_', '-', $url);
    return $url;
  }

  //convert from url friendly
  function fromUrlFriendly($url)
  {
    $url = str_replace('-', '_', $url);
    return $url;
  }

  //sanitize inputed data
  function sanitize($data)
  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

  //sanitize inputed array data
  function sanitizeArray($data)
  {
    for ($i=0; $i < count($data); $i++) 
    { 
      $data[$i] = sanitize($data[$i]); 
    }
    return $data;
  }

  //returns date in dd/mm/yyyy format
  function formatDate($date)
  {
    $dt = explode('-', $date);
    $y = $dt[0]; $m = $dt[1]; $d = $dt[2];
    return $d .'/'. $m .'/'. $y;
  }


  //register a log entry
  function registerLog($type, $description)
  {
    try 
    {
      Log::register($type, $description);
    } 
    catch (Illuminate\Database\QueryException $e) 
    {
      $message = $e->getMessage();
      echo $message;
      exit();
    }
  }

  
  //database error checking function
  function checkDatabaseError($e)
  {
    $message = 'Something went wrong. Please contact support.';
    $message_type = ERROR_MESSAGE;
    
    registerLog(ERROR_LOG, $e->getMessage());

    var_dump($e->getMessage());
    exit();

    $msg = ['message' => $message, 'message_type' => $message_type];

    return $msg;
  }


  //verify that record exists in database
  function checkRecordExists($model, $fieldName, $value)
  {
    $exists = false;
    try 
    {
      $model::where($fieldName, '=', $value)->firstOrFail();
      $exists = true;
    } 
    catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) 
    {
       $exists = false;
    }
    return $exists;
  }


  //fail safe
  function pageFailSafe()
  {
    redirectTo('dashboard');
  }


  //check for available form data
  function checkFormData($formName)
  {
    var_dump($_COOKIE);exit();
    return isset($_COOKIE[$formName]); 
  }


  //retrieve submited form data
  function getFormData($formName)
  {
    $data;

    if (checkFormData($formName)) 
    {
      //get form data from cookie
      $data = $_COOKIE[$formName];
      //convert from JSON format
      $data = json_decode($data);
    } 
    else
    {
      pageFailSafe();
    }

    return $data;
  }


  //check for available view data
  function checkViewData($slug)
  {
    return isset($_COOKIE[$slug]);
  }


  //set data for current view
  //receives an array and stores it in a cookie
  function setViewData($data, $slug = 'data')
  {
    if (checkViewData($slug)) 
    { 
      clearViewData($slug); 
    }
    $view_data = json_encode($data);
    setcookie($slug, $view_data, time() + 3600, '/');
  }


  //get data for current view
  function getViewData($slug = 'data')
  {
    $data = [];
    if (checkViewData($slug)) 
    {
      $data = json_decode($_COOKIE[$slug]);
    }
    return $data;
  }


  //clear view data
  function clearViewData($slug)
  {
    setcookie($slug, '', time() - 3600, '/');
  }


  //check for available view message
  function checkViewMessage()
  {
    return isset($_COOKIE['message']); 
  }


  //set message for current view
  //after form submission
  function setViewMessage($message, $type)
  {
    if (checkViewMessage()) 
    { 
      clearViewMessage(); 
    }
    $message = ['message' => $message, 'message_type' => $type];
    $message = json_encode($message);
    setcookie('message', $message,  time() + 4200, '/');
  }


  //get data for current view
  function getViewMessage()
  {
    $message = ['message' => '', 'message_type' => ''];
    if (checkViewMessage()) 
    {
      $message = json_decode($_COOKIE['message']);
      $message = [
        'message' => $message->message, 
        'message_type' => $message->message_type
      ];
    }
    clearViewMessage();
    return $message;
  }


  //clear view message
  function clearViewMessage()
  {
    setcookie('message', '', time() - 3600, '/');
  }

  //check if admin is already logged in
  function is_logged_in() 
  {
    return isset($_SESSION["id"]);
  }

  //check user inactivity
  function is_inactive() 
  {
    //1800 equals 30 minutes
    if ((time() - $_SESSION["last_active"]) > 1800)
    { return true; } 
    else 
    { return false; }
  }

  //reset user session for reauthentication
  function resetUserSession()
  {
    $_SESSION["last_active"] = 500;
  }

  //if user session is still valid
  //add to user minutes
  function add_time() 
  {
    $_SESSION["last_active"] = time();
  }

  function is_otp_sent() 
  {
    return $_SESSION["otp_sent"];
  }

  function set_otp_sent() 
  {
    $_SESSION["otp_sent"] = true;
    $_SESSION["last_otp"] = time() - 700;
  }

  function otp_sent() 
  {
    $_SESSION["last_otp"] = time();
  }

  function otp_expired() 
  {
    //600 equals 10 minutes
    if ((time() - $_SESSION["last_otp"]) > 600)
    { return true; } 
    else 
    { return false; }
  }

  function init_email_sent() 
  {
    $_SESSION["email_sent"] = false;
    $_SESSION["last_email"] = time();
  }

  function is_email_sent() 
  {
    return $_SESSION["email_sent"];
  }

  function set_email_sent() 
  {
    $_SESSION["email_sent"] = true;
    $_SESSION["last_email"] = time() - 1900;
  }

  function email_sent() 
  {
    $_SESSION["last_email"] = time();
  }

  function email_expired() 
  {
    if ((time() - $_SESSION["last_email"]) > 1800)
    { return true; } 
    else 
    { return false; }
  }


  //generate csrf token fo form validation
  function generateCsrf()
  {
    $csrf_token = bin2hex(random_bytes(32));
    return $csrf_token;
  }


  //get date, time and timezone
  //for record purposes
  function logTimestamp()
  {
    $timestamp = date("D d F, Y h:i:sa O");
    return $timestamp;
  }

  //get the current user's id
  function getUserId()
  {
    return $_SESSION["id"];
  }


  //get the current user's id
  function getUserUname()
  {
    return $_SESSION["username"];
  }


  //get the current user's id
  function getUserFullName()
  {
    return $_SESSION["full_name"];
  }

  function getUserEmail()
  {
    return $_SESSION["email"];
  }

  function getUserRole()
  {
    return $_SESSION["role"];
  }

  function getUserAcBalance()
  {
    return $_SESSION["account_balance"];
  }

  function refreshAcBalance()
  {
    $_SESSION["account_balance"] = UserAccount::getAccountBalance(getUserId());
  }


  //get the current user's last login
  function getUserLastLogin()
  {
    return $_SESSION["last_logged_in"];
  }


  function is_an_admin()
  {
    return ($_SESSION['role'] == 1) ? true : false;
  }

  function is_current_user($id)
  {
    return ($_SESSION['id'] == $id) ? true : false;
  }

  //pagination for query results
  function paginate($page, $records)
  {
    $page = (int) $page;

    $page_info = $data = []; 
    $number_of_records = $number_of_pages = $previous_page = 0;

    $number_of_records = $records;

    $number_of_pages = ceil($number_of_records / RECORDS_PER_PAGE);

    if ($page < 1) 
    { $page = 1; }

    if ($page > $number_of_pages) 
    { $page = $number_of_pages; }

    $page_info = [
      'current-page' => $page,
      'records-per-page' => RECORDS_PER_PAGE,
      'total-records' => $number_of_records,
      'total-pages' => $number_of_pages
    ];

    $previous_page = ($page - 1) * RECORDS_PER_PAGE;
             
    $result = [ 
      'page_info' => $page_info,
      'previous_page' => $previous_page
    ];

    return $result;
  }


  //hashing function
  function encryptPassword($password)
  {
    $encrypted = password_hash($password, PASSWORD_DEFAULT);

    return $encrypted;
  }


  //un-hash function
  function decryptPassword($pwd, $password)
  {
    $decrypted = password_verify($pwd, $password);

    return $decrypted;
  }


  //generate default password
  function generatePassword()
  {
    $length = 12;
    $characters = '0123456789987654321*&#@!$abcdefghijklmnopqrstuvwxyz*&#@!$ABCDEFGHIJKLMNOPQRSTUVWXYZ*&#@!$';
    $charactersLength = strlen($characters);
    $password = '';
    for ($i=0; $i < $length ; $i++) 
    { 
      $password .= $characters[rand(0, $charactersLength - 1)];
    }
    return $password;
  }

  function generateDays($number, $hours=0, $minutes=0)
  {
    $days = [];
    if ($number < 1) 
    {
      $days[0] = date("F d, Y, h:i", strtotime("+" . $minutes . " min"));
    } 
    else 
    {
      for ($i=1; $i <= $number; $i++) 
      { 
          $randhour = generate_random_hour();
          $randommin = generate_random_min();
          $day = date("F d, Y, h:i a", strtotime("+" .$i. " days +".$randhour." +".$randommin));
          $days[$i] = $day;
      }
    }
    
    return $days;
  }

  function generate_serial_number ($length = 20) 
  {
    $characters = '0123456789987654321ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $trackingNumber = '';
    for ($i=0; $i < $length ; $i++) { 
      $trackingNumber .= $characters[rand(0, $charactersLength - 1)];
    }
    return $trackingNumber;
  }

  function generate_random_hour () {
    $length = 1;
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomTime = '';
    for ($i=0; $i < $length ; $i++) { 
      $randomTime .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomTime;
  }

  function generate_random_min () {
    $length = 2;
    $characters = '012345678909876543210';
    $charactersLength = strlen($characters);
    $randomTime = '';
    for ($i=0; $i < $length ; $i++) { 
      $randomTime .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomTime;
  }

  function transactionStatus($stat)
  {
    $status = '';

    switch ($stat)
    {
      case '0':
        $status = 'Pending';
        break;

      case '1':
        $status = 'Approved';
        break;

      case '2':
        $status = 'Declined';
        break;
      
      default:
        $status = 'Pending';
        break;
    }

    return $status;
  }

  function transactionStatStyle($stat)
  {
    $status = '';

    switch ($stat)
    {
      case '0':
        $status = 'text-info';
        break;

      case '1':
        $status = 'text-success';
        break;

      case '2':
        $status = 'text-danger';
        break;
      
      default:
        $status = 'text-info';
        break;
    }

    return $status;
  }

  function has_min_length($string)
  {
    return strlen($string) >= 6 ? true : false;
  }

  function has_max_length($string)
  {
    return strlen($string) >= 12 ? true : false;
  }

  function closeDueInvestments()
  {
    if (UserInvestment::hasInvestment(getUserId())) 
		{
			$investments = UserInvestment::getUserInvestments(getUserId());

			foreach ($investments as $investment) 
			{
				$q = strtotime($investment->end_date);
				
				if (strtotime(date("F d, Y")) >= strtotime(date("F d, Y", $q))) 
				{
					if ($investment->status === 'open') 
					{
						UserInvestment::closeInvestment($investment->id);
						$bal = $investment->amount + $investment->profit;
						UserAccount::updateAccountBalance('credit', $investment->user_id, $bal);
						UserTransaction::makeTransaction($investment->user_id, 'investment',
						'Investment of $'.number_format($investment->amount).' closed with $'.number_format($investment->profit).' profit. Account credited.', $bal);
					}
				}
			}
		}
  }

  function closeAllDueInvestments()
  {
    $investments = UserInvestment::getOpenInvestments();

    foreach ($investments as $investment) 
    {
        $q = strtotime($investment->end_date);

        if (strtotime(date("F d, Y")) >= strtotime(date("F d, Y", $q))) 
        {
          UserInvestment::closeInvestment($investment->id);
          $bal = $investment->amount + $investment->profit;
          UserAccount::updateAccountBalance('credit', $investment->user_id, $bal);
        }
    }
  }
  
 	
 	
?>