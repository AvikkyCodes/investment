<footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; <?php echo Date("Y"); ?> Henley Trade Global | All rights reserved.</span>
          </div>
        </div>
      </footer>

      <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?php require_once 'logout_prompt.php'; ?>


  <!-- Delete Modal-->

  

  <!-- Bootstrap core JavaScript-->

  <script type="text/javascript" src="vendor/components/jquery/jquery.min.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

  <script type="text/javascript" src="vendor/components/bootstrap/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="js/sb-admin-2.js"></script>

  <script src="datatables/jquery.dataTables.min.js"></script>
  
  <script src="datatables/dataTables.bootstrap4.min.js"></script>

  <script src="js/demo/datatables-demo.js"></script>

  <script type="text/javascript" src="vendor/onokumus/metismenu/dist/metisMenu.min.js"></script>

  <!-- Page level plugins -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.0/chart.min.js"></script> 
  
  
  <script type="text/javascript">
    $(document).ready( function () {
    $('#myTable').DataTable();
} );
  </script>

  <!-- session management -->
  <?php //require_once 'session_script.php'; ?>