<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Refer Users';
    require_once 'resources/header.php'; 
    require_once 'resources/table_style.php'; 
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		Refer Users To Henley Traders Community
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                <?php require_once 'resources/form_sub_msg.php'; ?>

                <div class="form-group text-center">
                  <h5><strong>You can refer users by sharing your referral link</strong></h5>
                  <div class="mt-1 form-row">
                    <div class="col-md-6 offset-md-3">
                      <div class="mb-3 input-group" >
                        <input type="text" class="form-control myInput readonly text-dark bg-light" value="<?=$data['user']['referral_link']?>" id="myInput" readonly style="max-width: 500px;">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" onclick="myFunction()" type="button" id="button-addon2"><i class="fas fa-copy"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>

                    <h5><strong>Or your referral ID</strong></h5>
                    <h3 class="text-primary"><strong><?=$data['user']['referral_id']?></strong></h3>
                    <br>
                    <?php if($data['user']['referred_by']): ?>
                      <h5 class="text-dark"><small>You were referred by</small></h3>
                      <i class="fa fa-user fa-2x"></i>
                      <h6 class="text-primary"><?=$data['user']['referred_by']?></h6>
                    <?php endif; ?>
                </div>

                

                <div class="table-responsive">
                  <h2>Your Referrals</h2>
                  <bold class="text-primary">(Referral Level: <?=$data['user']['referral_level']?>)</bold> 
                  <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              
                      <thead>
                      <tr id="mytable">
                          <th id="mytable">Client Name</th>
                          <th id="mytable">Client Status</th>
                          <th id="mytable">Referral Bonus ($)</th>
                          <th id="mytable">Date Registered</th>
                      </tr>
                      </thead>

                      <tbody>

                      <?php 
                          foreach ($data['referrals'] as $referral):
                      ?>
                          <tr id="mytable">
                              <td id="mytable">
                                  <?=ucfirst($referral->full_name)?>
                              </td>

                              <td id="mytable" class="<?=$referral->active ? 'text-success' : 'text-danger'; ?>">
                                  <?=$referral->active ? 'Active' : 'Disabled'; ?>
                              </td>

                              <td id="mytable">
                                  <?=number_format($referral->referral_bonus)?>
                              </td>

                              <td id="mytable">
                                  <?=$referral->date_registered?>
                              </td>
                          </tr>
                      <?php endforeach; ?>
                      </tbody>
                  </table>
              </div>
                    
                  
                </div>
            </div>
          </div>

          <script>
            function myFunction() {
              /* Get the text field */
              var copyText = document.getElementById("myInput");
              /* Select the text field */
              copyText.select();
              copyText.setSelectionRange(0, 99999); /* For mobile devices */
              /* Copy the text inside the text field */
              document.execCommand("copy");
              /* Alert the copied text */
              alert("Referral link copied: " + copyText.value);
              }
          </script>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>