<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Investment Details';
    require_once 'resources/header.php';
  ?>

</head>

<body id="page-top">

  
  <main class="main" id="top">
    
    <?php require_once 'resources/navbar.php'; ?>

      <section class="py-xxl-10 pb-0" id="home">
        <div class="container">
          <div class="row align-items-center">
            <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                        Investment Details
                    </h1>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                      
                      <?php require_once 'resources/form_sub_msg.php';?>

                    <div class="row">
                        <div class="col-md-5">
                        <div>
                <b style="font-size: 24px;" class="mr-3">Plan:</b> 
            <i class="text-lg text-primary">
              <b><?=ucfirst($data['investment']->plan) ?></b>
            </i>
                <p><b class="mr-3">Start Date :</b> <?=$data['investment']->start_date; ?>
                <br>
                <b class="mr-3">End Date :</b> <?=$data['investment']->end_date; ?></p>
                <b class="mr-3">Status : </b>
                <b class="text-lg <?=$data['investment']->status == 'open' ? 'text-success' : 'text-danger'; ?>">
                  <?=ucfirst($data['investment']->status); ?>
                </b>
              </div>
                        </div>
                        <div class="col-md-5">

                        <div>
                <b class="text-lg mr-3">Amount:</b> 
              <b class="text-lg text-primary">$<?=number_format($data['investment']->amount) ?></b>
              <br>
              <b class="text-lg mr-3">Daily Interest:</b> 
              <b class="text-lg text-primary">$<?=number_format($data['investment']->interest) ?></b>
              <br>
              <b class="text-lg mr-3">Expected Profit:</b> 
              <b class="text-lg text-primary">$<?=number_format($data['investment']->profit) ?></b>
             
              </div>
                        </div>
                    </div>
                    

                    <div class="row mt-3">
          <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  <tr>
                    <th id="mytable">Date & Time</th>
                    <th id="mytable">Remarks</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                    foreach ($data['tracking'] as $track) {
                      $q = strtotime($track->dateandtime);
                      if (strtotime(date("F d, Y")) >= strtotime(date("F d, Y", $q))) 
                      {
                  ?>
                  <tr>
                    <td id="mytable"><?=$track->dateandtime ?></td>
                    <td id="mytable"><?=$track->remarks ?></td>
                  </tr>
                  <?php
                      }
                    } 
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
                </div>
          </div>

                  </div>
      </section>

    <?php require_once 'resources/footer.php'; ?>

  </main>


</body>

</html>
