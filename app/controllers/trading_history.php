<?php

Class Trading_History extends Controller
{

	public function index()
	{
		$investments = UserInvestment::getClosedUserInvestments(getUserId());
		$this->view('trading_history',
					[
						'investments' => $investments,
						'message' => '',
						'message_type' => ''
					]
				);
	}
	
} 

