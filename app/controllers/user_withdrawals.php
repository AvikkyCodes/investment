<?php

Class User_Withdrawals extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		$msg = getViewMessage();
        $message = $msg['message']; $message_type = $msg['message_type'];
		$withdrawals = UserWithdrawal::orderBy('id', 'desc')->get();
        
		$this->view('withdrawals',
					[
						'title' => 'All User Deposits',
                        'withdrawals' => $withdrawals,
						'message' => $message,
						'message_type' => $message_type
					]
				);
	}
	
} 

