

<div class="modal fade" id="recycleBinModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure about this?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
          <i>
            Warning! <br> 
            You are about to empty the recycle bin. 
            <br>
            <span class="mt-2 text-danger"> 
              <b>
                Records will be deleted permanently. This action cannot be undone.
              </b>
            </span>
          </i>
        </div>
        
        <div class="alert alert-info">
          For security reasons, we need to verify your identity. 
          <br>
          Please enter your password to proceed.
        </div>

      </div>
      <div class="modal-body">

        <form method="POST" class="form-stacked p-10" 
        action="empty-user-recycle-bin" >

          <div class="form-group">
            <div class="float-left">
              <input type="password" name="pwd" class="form-control"
              placeholder="enter your password" value="" required>

              <input type="hidden" name="location" class="form-control"value="<?=$redirect_location?>" >
            </div>
          </div>

          <div class="form-group mt-5 float-right">
            <div class="">
              <div class="">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <input class="btn btn-danger" type="submit" value="proceed">
              </div>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>