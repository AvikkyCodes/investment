<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

Class Database
{

	protected $capsule;

	public function __construct()
	{
		$this->capsule = new Capsule();
		$conn = [
			"driver"	=> "mysql",
			"host" 		=> 'localhost',
			"database" 	=> 'investment',
			"username" 	=> 'root',
			"password" 	=> '',
			"charset" 	=> "utf8",
			"collation" => "utf8_unicode_ci",
			"prefix" 	=> "",
			"engine"	=> "innodb"
		];
		$this->capsule->addConnection($conn);
		$this->capsule->setEventDispatcher(new Dispatcher(new Container));
		$this->capsule->setAsGlobal();
		$this->capsule->bootEloquent();
	}


	//get database connection instance
	public function getCapsuleConnection()
	{
		return $this->capsule->getConnection();
	}


}


