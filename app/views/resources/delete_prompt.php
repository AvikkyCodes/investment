<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure about this?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
          <i>
            Warning! <br> 
            You are about to delete one 
            "<?=ucwords($delete_model)?>" record.
            <br>
            <span class="mt-2"> 
              <b id="note">Record will be sent to the recycle bin.</b>
            </span>
          </i>
        </div>
        
        <div class="alert alert-info">
          For security reasons, we need to verify your identity. 
          <br>
          Please enter your password to proceed.
        </div>

      </div>
      <div class="modal-body">

        <form method="POST" class="form-stacked p-10" 
        action="delete_record" >

          <div class="form-group mr-4">
            <input type="checkbox" id="checkRecycle" onclick="toggleRecycle()" class="mr-1"> 
              <i class="text-danger">Delete permanently?</i>
          </div>

          <div class="form-group">
            <div class="float-left">
              <input type="password" name="pwd" class="form-control"
              placeholder="enter your password" value="" required>
            </div>
          </div>

          <div class="form-group">
            <input type="hidden" name="id" value="<?=$delete_id?>">
            <input type="hidden" name="model" value="<?=$delete_model?>">
            <input type="hidden" name="location" value="<?=$redirect_location?>">
            <input type="hidden" name="recycle" id="setRecycle" value="true">
          </div>

          <div class="form-group mt-5 float-right">
            <div class="">
              <div class="">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <input class="btn btn-danger" type="submit" value="delete">
              </div>
            </div>
          </div>

        </form>
        <script type="text/javascript">
          function toggleRecycle()
          {
            x = document.getElementById("checkRecycle").checked;

            if (x) 
            {
              document.getElementById("note").innerHTML = "Record will be deleted permanently. This action cannot be undone.";
              document.getElementById("note").className = "text-danger";
              document.getElementById("setRecycle").value = "false";
            }
            else
            {
              document.getElementById("note").innerHTML = "Record will be sent to the recycle bin.";
              document.getElementById("note").className = "";
              document.getElementById("setRecycle").value = "true";
            }

          }
        </script>

      </div>
    </div>
  </div>
</div>