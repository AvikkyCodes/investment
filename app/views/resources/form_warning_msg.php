<div class="form-group">
	<div class="form-row">
		<div class="col-md-12">
			<div class="form-sub-heading alert alert-warning">
				<p class="text-center font-weight-bold">
	              DO NOT RELOAD THIS PAGE UNTIL THE FORM HAS BEEN SUBMITED.
	              <br>
	              RELOADING THE PAGE MAY CAUSE YOU TO LOSE ALREADY INPUTED DATA.
          		</p>
          		<div class="text-center alert alert-info">
			      <p>
			        USE THE <span class="font-italic"><mark>"NEXT"</mark></span> AND <span class="font-italic"><mark>"PREVIOUS"</mark></span> BUTTONS TO 
			        NAVIGATE THROUGH THE FORM PAGES.
			    </div>
			</div>
		</div>
	</div>
</div>