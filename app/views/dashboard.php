<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Dashboard';
    require_once 'resources/header.php'; 
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php 
      require_once 'resources/sidebar.php'; 
    ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between mb-1">
              <h1 class="m-0 font-weight-bold text-primary"> DASHBOARD</h1>
            </div>
          </div>

          <?php 
            require_once 'resources/form_sub_msg.php';

            if (is_an_admin()) 
            {
              require_once 'resources/dashboards/admin_dashboard.php';
            }
            else 
            {
              require_once 'resources/dashboards/user_dashboard.php';
            }
            
          ?>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>
