<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from orchardcapitaltraders.com/contact by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2022 09:02:07 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="LlJDpqNPeWhT92XG7PKkbav1uBTHsSMPoS3a1U2g">
        <title>Henley Trade Global | Contact us</title>
        
        <meta name="description"
            content="We offers Crypto Assets Management of the highest quality on the basis of foreign exchange and profitable trade through Bitcoin exchanges. There is no other worldwide financial market that can guarantee a daily ability to generate constant profit with the large price swings of Bitcoin.">
        
        <!-- Google / Search Engine Tags -->
        <meta itemprop="name" content="HenleyTradeGlobal - Welcome to HenleyTradeGlobal">
        <meta itemprop="description"
            content="We offers Crypto Assets Management of the highest quality on the basis of foreign exchange and profitable trade through Bitcoin exchanges. There is no other worldwide financial market that can guarantee a daily ability to generate constant profit with the large price swings of Bitcoin.">
        <meta itemprop="image" content="temp/images/meta.html">
        
        <link rel="icon" href="storage/app/public/photos/rCyrNPorchardcapitaltraders_logo.html" type="image/png"/>
                    
            <link href="temp/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- Icons -->
            <link href="temp/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        
            <!-- Magnific -->
            <link rel="stylesheet" href="temp/css/line.css">
            <link href="temp/css/flexslider.css" rel="stylesheet" type="text/css" />
            <link href="temp/css/magnific-popup.css" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css')}}" />
        
            <!-- Slider -->
            <link rel="stylesheet" href="temp/css/owl.carousel.min.css" />
            <link rel="stylesheet" href="temp/css/owl.theme.default.min.css" />
            <!-- Main Css -->
            <link href="temp/css/style.css" rel="stylesheet" type="text/css" />
            <link href="temp/css/colors/default.css" rel="stylesheet">
           <!-- <link rel="stylesheet" type="text/css" href="mulla/style.css">-->
        

    </head>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/6323372537898912e96960d6/1gd0ourl8';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    <body>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        {tawk to codess}
        </script>
       
        
    <!-- Navbar STart -->
    <header id="topnav" class="sticky defaultscroll bg-white">
        <div class="container">
            <!-- Logo container-->
            <div>
                <a class="logo" href="/">
                    <img src="images/Henley Trade.jpg" height="35" alt="" class="mr-2">
                </a>
            </div>
            <div class="buy-button">

                <a href="login" class="mr-3 btn btn-warning login-btn-success">Login</a>
                <a href="register" class="btn btn-warning login-btn-success ">Get Started</a>


            </div>
            <!--end login button-->
            <!-- End Logo container-->
            <div class="menu-extras">
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

            <div id="navigation" class="">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li><a href="/">Home</a></li>
                    <li><a href="about">About us</a></li>
            
                    <li><a href="faq">Faq</a></li>
                    <li><a href="contact">Contact</a></li>

                </ul>


                </ul>
                <!--end navigation menu-->
                <div class="buy-menu-btn d-none">
                    <a href="login" target="_blank" class="btn btn-warning">Login</a>
                    <a href="register" target="_blank" class="btn btn-warning">Get Started</a>
                </div>
                <!--end login button-->
            </div>
            <!--end navigation-->
        </div>
        <!--end container-->
    </header>
    <!--end header-->
    <!-- Navbar End -->
    
    
    <!-- Hero Start bg-half cont d-table w-100-->
    <section class=" cont bg-half bg-dark w-100 d-table">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="text-center  col-lg-12">
                    <div class="page-next-level">
                        <h4 class="title text-light fs-1 fw-bolder">Contact Us</h4>
                        <div class="page-next">
                            <nav aria-label="breadcrumb" class="d-inline-block ">
                                <ul class="mb-0 bg-warning rounded shadow breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="text-white">Henley Trade Global</a>
                                    </li>

                                    <li class="breadcrumb-item text-dark " aria-current="page">Contact Us</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="overflow-hidden text-white shape">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Start Contact -->
    <section class="pb-4 section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="text-center border-0 card features feature-clean">
                        <div class="mx-auto text-center icons text-primary">
                            <i class="mb-0 text-warning bg-dark rounded rounded uil uil-phone d-block h3"></i>
                        </div>
                        <div class="mt-3 content">
                            <h5 class="font-weight-bold ">Phone Number</h5>

                            <a href="tel:+44 xxxxxxxx"
                                class="text-dark">+44 xxxxxxxx</a>
                        </div>
                    </div>
                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-md-4 mt-sm-0 pt-sm-0">
                    <div class="text-center border-0 card features feature-clean">
                        <div class="mx-auto text-center icons text-primary">
                            <i class="mb-0 bg-dark text-warning rounded uil uil-envelope d-block h3"></i>
                        </div>
                        <div class="mt-3 content">
                            <h5 class="font-weight-bold">Email</h5>

                            <a href="mailto:support@HenleyTradeGlobal.com"
                                class="text-dark">support@HenleyTradeGlobal.com</a>
                        </div>
                    </div>
                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-md-4 mt-sm-0 pt-sm-0">
                    <div class="text-center border-0 card features feature-clean">
                        <div class="mx-auto text-center icons ">
                            <i class="mb-0 text-warning bg-dark rounded rounded uil uil-map-marker  d-block h3"></i>
                        </div>
                        <div class="mt-3 content">
                            <h5 class="font-weight-bold">Address</h5>
                            <p class="text-muted text-dark">50 Grosvenor Hill. W1K 3QY London.</p>

                        </div>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
        </div>
        <!--end container-->



    
    
    
    
    <!-- Footer Start -->
    <footer class="footer bg-dark mb-0">
        <div class="container">
            <div class="row">
                <div class="pb-0 mb-0 col-lg-4 col-12 mb-md-4 pb-md-2">
                    <h5 class="text-light footer-head">Henley Trade Global</h5>
                    <p class="mt-4">We offer Crypto Assets Management of the highest quality on the basis of foreign exchange and profitable trade through Bitcoin exchanges. There is no other worldwide financial market that can guarantee a daily ability to generate constant profit with the large price swings of Bitcoin.</p>

                    <ul class="mt-4 mb-0 list-unstyled social-icon social">
                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                    data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                    data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                    data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                    </ul>
                    <!--end icon-->

                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-lg-4 col-md-4 mt-sm-0 pt-sm-0">
                    <h5 class="text-light footer-head">Useful Links</h5>
                    <ul class="mt-4 list-unstyled footer-list">
                        <li><a href="/" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                Home</a></li>
                        <li><a href="about" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                About us</a></li>
                        <li><a href="contact" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                Contact Us</a></li>
                        <li><a href="faq" class="text-foot"><i class="mr-1 mdi mdi-chevron-right"></i>
                                Faq</a></li>
                    </ul>
                </div>
                <!--end col-->

                <div class="pt-2 mt-4 col-lg-4 col-md-4 mt-sm-0 pt-sm-0">
                    <h5 class="text-light footer-head">Contact Details</h5>
                    <div class="mt-2">
                        <h6 class="text-foot"><i class="mr-1 mdi mdi-home"> </i>
                            Head Office</h6>
                        <p>50 Grosvenor Hill. W1K 3QY London.</p>
                        <h6><i class="mr-1 mdi mdi-email"> </i>Email Address</h6>
                        <p>support@HenleyTradeGlobal.com</p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </footer>
    <!--end footer-->
    <footer class="footer footer-bar bg-light">
        <div class="container text-center">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="text-sm-left">
                        <p class="mb-0 text-dark text-center ">
                            © Copyright 2022 OCTraders All
                            Rights Reserved.
                        </p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </footer>
    <!--end footer-->
    <!-- Footer End -->
    
    

                  <!-- javascript -->
            <script src="temp/js/jquery-3.5.1.min.js"></script>
            <script src="temp/js/bootstrap.bundle.min.js"></script>
            
            <!-- SLIDER -->
            <script src="temp/js/owl.carousel.min.js"></script>
            <script src="temp/js/owl.init.js"></script>
            <!-- Icons -->
            <script src="temp/js/feather.min.js"></script>
            <script src="temp/js/bundle.html"></script>
            
            <script src="temp/js/app.js"></script>
            <script src="temp/js/widget.js"></script>
       

    </body>

<!-- Mirrored from orchardcapitaltraders.com/contact by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2022 09:02:07 GMT -->
</html>
