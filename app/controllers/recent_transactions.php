<?php

class Recent_Transactions extends Controller
{

    public function index()
    {  
        $deposits = RecentTransaction::getRecentDeposits();
        $withdrawals = RecentTransaction::getRecentWithdrawals();

        closeAllDueInvestments();


        $this->view("recent_transactions", 
            [
                'deposits' => $deposits,
                'withdrawals' => $withdrawals,
                'message' => '',
                'message_type' => ''
            ]
        );
    }
    

}

?>