<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
    $page_title = 'Make Payment'; 
    require_once 'resources/header.php';
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

   <?php require_once 'resources/sidebar.php'; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
        <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <div class="row">

              <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                        Make Payment
                    </h1>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                  

                        <?php require_once 'resources/form_sub_msg.php';?>
                        
                        <h4 class="text-dark">
                          <ul>
                            <li>
                              You are to make payment of <strong>$<?=number_format($data['amount'])?></strong> 
                              using your selected payment method.
                            </li>
                            <li>Screenshot and upload the proof of payment</li>
                          </ul>
                        </h4>
											  <h4>
                          <img src="img/<?=$data['method']?>.png" alt="" width="100" height="100" >
                          <strong class="text-dark"><?=$data['method'] == 'btc' ? 'Bitcoin' : 'Ethereum' ?></strong>
											  </h4>
                        
                        <div class="mt-5">
                          <h3 class="text-dark">
													  <strong>
                              <?=$data['method'] == 'btc' ? 'Bitcoin' : 'Ethereum' ?> Address:
                            </strong>
												  </h3>
												  <div class="form-group">
    												<div class="mb-3 input-group">
    													<input type="text" class="form-control myInput readonly text-dark bg-light" value="<?=$data['address']?>" id="myInput" readonly style="max-width: 500px;">
    													<div class="input-group-append">
    														<button class="btn btn-outline-secondary" onclick="myFunction()" type="button" id="button-addon2"><i class="fas fa-copy"></i></button>
    													</div>
    												</div>
    												<h5><strong>Network Type:</strong><small class="text-dark"> Erc</small></h5>
												  </div>
												</div>


                        <div class="form-horizontal">
                          <form method="post" action="do_deposit" enctype="multipart/form-data">													
                            <input type="hidden" name="amount" value="<?=$data['amount']?>">
                            <input type="hidden" name="payment_method" value="<?=$data['method']?>">
                            <div class="form-group">
                              <h5 class="text-dark">Upload Proof of Payment.</h5>
                              <input type="file" name="image" class="form-control col-lg-4 bg-light text-dark" required>
                            </div>

                            <div class="form-group">
                              <input type="submit" class="btn btn-primary" value="Submit Payment">
                            </div> 
                          </form>
                        </div>

                  </div>
                 </div>
               </div>
             </div>

             <script>
                function myFunction() {
                  /* Get the text field */
                  var copyText = document.getElementById("myInput");
                  /* Select the text field */
                  copyText.select();
                  copyText.setSelectionRange(0, 99999); /* For mobile devices */
                  /* Copy the text inside the text field */
                  document.execCommand("copy");
                  /* Alert the copied text */
                  alert("Address copied: " + copyText.value);
                  }
              </script>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php';
      ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->


</body>

</html>
