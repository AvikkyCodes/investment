<?php

Class User_Referrals extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		
        $msg = getViewMessage();
        $user_referrals = [];
        $users = User::active();
        foreach ($users as $user) 
        {
            if (User::hasReferrals($user->referral_id)) 
            {
                $ref = User::getUserReferrals($user->referral_id);
                $user_referrals[$user->email] = ['user' => $user->email, 'referrals' => $ref];
            }
        }

		$this->view('user_referrals',
					[
                        'user_referrals' => $user_referrals,
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

