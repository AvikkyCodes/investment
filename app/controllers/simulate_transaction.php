<?php

Class Simulate_Transaction extends Controller
{

	public function index($type="", $name="", $amount="")
	{
        if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
        
		$message = ''; $message_type = SUCCESS_MESSAGE;

		try 
		{
			RecentTransaction::makeTransaction($type, $name, $amount);
            $message = 'Transaction made successfully.';
            registerLog(ACTIVITY_LOG, getUserEmail().' made a '. $type . ' transaction.');
            
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo('simulate');
	}
	
} 

