<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'User Withdrawals';
    require_once 'resources/header.php'; 
    require_once 'resources/table_style.php'; 
  ?>

  <style type="text/css">

    #tab {
      padding: 10px;
      margin: 10px;
    }

    .nav {
        padding: 10px;
    }

    .nlink {
        padding: 10px;
        margin: 20px;
        margin-top: 0px;
        border-radius: 5px;
        font-size: 25px;
        font-weight: bold;
        border: 1px solid black;
    }

  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		All User Withdrawals
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                <?php require_once 'resources/form_sub_msg.php'; ?>

                <div style="overflow:auto;" class="mt-1">
                    <div style="float:right;">
                        <div style="display:inline-block;margin-right:10px;">
                            <form action="process_multiple/withdrawal" onsubmit="return processMultiple(this)" class="form-inline">
                                <input type="hidden" name="type" value="approve">
                                <input type="hidden" name="reason" value="">
                                <button type="submit" id="multiple1" class="btn btn-success" disabled>Approve Multiple</button>
                            </form>
                        </div>

                        <div style="display:inline-block">
                            <form action="process_multiple/withdrawal" onsubmit="return processMultiple(this)">
                                <input type="hidden" name="type" value="decline">
                                <textarea name="reason" id="reason" class="form-control mb-1" cols="10" rows="1" required disabled placeholder="reason"></textarea>
                                <button type="submit" id="multiple2" class="btn btn-danger" disabled>Decline Multiple</button>
                            </form>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                            
                                    <thead>
                                    <tr id="mytable">
                                        <th id="mytable">Select</th>
                                        <th id="mytable">User</th>
                                        <th id="mytable">Amount ($)</th>
                                        <th id="mytable">Receiving Mode</th>
                                        <th id="mytable">Address</th>
                                        <th id="mytable">Status</th>
                                        <th id="mytable">Date Created</th>
                                        <th id="mytable">Approve</th>
                                        <th id="mytable">Decline</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php 
                                        foreach ($data['withdrawals'] as $withdrawal):
                                    ?>
                                        <tr id="mytable">
                                            <td id="mytable">
                                                <input type="checkbox" 
                                                    class="form-check-input" 
                                                    value="<?=$withdrawal->id.','.$withdrawal->amount?>"
                                                    name="withdrawal"
                                                >
                                            </td>

                                            <td id="mytable">
                                                <?=User::getEmail($withdrawal->user_id)?>
                                            </td>

                                            <td id="mytable">
                                                <?=number_format($withdrawal->amount)?>
                                            </td>

                                            <td id="mytable">
                                                <?=strtoupper($withdrawal->receiving_mode)?>
                                            </td>

                                            <td id="mytable">
                                                <div class="input-group" >
                                                    <input type="text" class="form-control myInput 
                                                        readonly text-dark bg-light" 
                                                        value="<?=UserSetting::getUserWallet($withdrawal->user_id, $withdrawal->receiving_mode)?>" 
                                                        id="myInput" readonly style="width:200px;cursor:pointer;" data-toggle="tooltip" data-placement="top" 
                                                        title="<?=UserSetting::getUserWallet($withdrawal->user_id, $withdrawal->receiving_mode)?>">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" onclick="myFunction()" type="button" id="button-addon2"><i class="fas fa-copy"></i></button>
                                                    </div>
                                                </div>
                                            </td>

                                            <td id="mytable" class="<?=transactionStatStyle($withdrawal->status)?>">
                                                <?=transactionStatus($withdrawal->status)?>
                                            </td>

                                            <td id="mytable">
                                                <?=$withdrawal->date_created?>
                                            </td>

                                            <td id="mytable">
                                                <form method="POST" action="process-withdrawal" class="form-horizontal">
                                                    <input type="hidden" name="id" value="<?=$withdrawal->id?>">
                                                    <input type="hidden" name="user_id" value="<?=$withdrawal->user_id?>">
                                                    <input type="hidden" name="action" value="approve">
                                                    <input type="hidden" name="reason" value="">
                                                    <input type="hidden" name="amount" value="<?=$withdrawal->amount?>">
                                                    <button class="btn btn-success" type="submit"
                                                        <?=$withdrawal->status == 1 ? 'disabled' : ''?>
                                                    >
                                                        Approve
                                                    </button>
                                                </form>
                                            </td>

                                            <td id="mytable">
                                                <form method="POST" action="process-withdrawal" class="form-horizontal">
                                                    <input type="hidden" name="id" value="<?=$withdrawal->id?>">
                                                    <input type="hidden" name="user_id" value="<?=$withdrawal->user_id?>">
                                                    <input type="hidden" name="action" value="decline">
                                                    <textarea name="reason" class="form-control" cols="10" rows="1" required
                                                    <?=$withdrawal->status == 2 ? 'disabled' : ''?> placeholder="reason"></textarea>
                                                    <input type="hidden" name="amount" value="<?=$withdrawal->amount?>">
                                                    <button class="btn btn-danger mt-1" type="submit"
                                                    <?=$withdrawal->status == 2 ? 'disabled' : ''?>
                                                    >
                                                        Decline
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                    <script type="text/javascript">
                        // Select all checkboxes with the name 'settings' using querySelectorAll.
                        var checkboxes = document.querySelectorAll("input[type=checkbox][name=withdrawal]");
                        let selectedWithdrawals = []

                        /*
                        For IE11 support, replace arrow functions with normal functions and
                        use a polyfill for Array.forEach:
                        https://vanillajstoolkit.com/polyfills/arrayforeach/
                        */

                        // Use Array.forEach to add an event listener to each checkbox.
                        checkboxes.forEach(function(checkbox) {
                        checkbox.addEventListener('change', function() {
                            selectedWithdrawals = 
                            Array.from(checkboxes) // Convert checkboxes to an array to use filter and map.
                            .filter(i => i.checked) // Use Array.filter to remove unchecked checkboxes.
                            .map(i => i.value) // Use Array.map to extract only the checkbox values from the array of objects.
                            
                            if (selectedWithdrawals.length !== 0) {
                                document.getElementById("multiple1").disabled = false
                                document.getElementById("multiple2").disabled = false
                                document.getElementById("reason").disabled = false
                            }
                            else {
                                document.getElementById("multiple1").disabled = true
                                document.getElementById("multiple2").disabled = true
                                document.getElementById("reason").disabled = true
                            }
                        })
                        });

                        function processMultiple(form) 
                        {
                            var checkboxes = document.querySelectorAll("input[type=checkbox][name=withdrawal]");
                            let selectedWithdrawals = []

                            checkboxes.forEach(function(checkbox) {
                                selectedWithdrawals = 
                                Array.from(checkboxes) // Convert checkboxes to an array to use filter and map.
                                .filter(i => i.checked) // Use Array.filter to remove unchecked checkboxes.
                                .map(i => i.value) // Use Array.map to extract only the checkbox values from the array of objects.
                            })

                            //create data object with form elements
                            let data = {
                                "action": form.type.value,
                                "selected": selectedWithdrawals,
                                "reason": form.reason.value
                            };

                            //convert data object into JSON format
                            jsonData = JSON.stringify(data);
                            //save data in cookie
                            document.cookie = "multiple=" + jsonData;
                            //redirect to form's action page
                            window.location.href = form.getAttribute("action");
                            
                            //prevent form from submitting
                            return false;

                        }
                    </script>

                    <script>
                        function myFunction() {
                        /* Get the text field */
                        var copyText = document.getElementById("myInput");
                        /* Select the text field */
                        copyText.select();
                        copyText.setSelectionRange(0, 99999); /* For mobile devices */
                        /* Copy the text inside the text field */
                        document.execCommand("copy");
                        /* Alert the copied text */
                        alert("Address copied: " + copyText.value);
                        }
                    </script>
                  
                </div>
            </div>
          </div>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>