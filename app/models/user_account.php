<?php

class UserAccount extends Model
{
	protected static function getAccountBalance($id)
	{
		$user = UserAccount::where('user_id', '=', $id)->first();
		return $user['balance'];
	}

	protected static function updateAccountBalance($action, $id, $amount)
	{
		$balance = UserAccount::getAccountBalance($id);
		$newBalance = 0;
		$bal = (int) $balance;
		$amt = (int) $amount;
		$action == 'credit' ? $newBalance = $bal + $amt : $newBalance = $bal - $amt;
		UserAccount::where('user_id', '=', $id)
        ->update(['balance' => $newBalance]);
		if (is_current_user($id)) 
		{ refreshAcBalance(); }
	}

	protected static function hasInsufficientFunds($id, $amount)
	{
		$balance = UserAccount::getAccountBalance($id);
		return ($balance < $amount) ? true : false;
		
	}



	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}