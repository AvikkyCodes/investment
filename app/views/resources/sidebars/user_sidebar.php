      <!-- Heading -->
      <div class="sidebar-heading">
        Main-Functions
      </div>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Nav Item -->
      <li class="nav-item">
          <a class="nav-link" href="trading-history">
            <i class="fas fa-fw fa-signal"></i>
            <span>Profit Record</span>
          </a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="transaction-history">
            <i class="fas fa-fw fa-clock"></i>
            <span>Transaction History</span>
          </a>
      </li>

      <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
              aria-expanded="true" aria-controls="collapseTwo">
              <i class="fas fa-fw fa-money-bill"></i>
              <span>Investment</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
              <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Investment:</h6>
                  <a class="collapse-item" href="join-plan">Subscribe To A Plan</a>
                  <a class="collapse-item" href="my-investments">My Investments</a>
              </div>
          </div>
      </li>
      
      
      <!-- Nav Item -->
      <li class="nav-item">
          <a class="nav-link" href="crypto-exchange">
            <i class="fas fa-fw fa-coins"></i>
            <span>Crypto Exchange</span>
          </a>
      </li>
      
      <!-- Nav Item -->
      <li class="nav-item">
          <a class="nav-link" href="transfer-funds">
            <i class="fas fa-fw fa-share"></i>
            <span>Transfer Funds</span>
          </a>
      </li>

      <!-- Nav Item -->
      <li class="nav-item">
          <a class="nav-link" href="refer-users">
            <i class="fas fa-fw fa-recycle"></i>
            <span>Refer Users</span>
          </a>
      </li>
      <!-- Nav Item -->
      <li class="nav-item">
          <a class="nav-link" href="support">
            <i class="fas fa-fw fa-life-ring"></i>
            <span>Help/Support</span>
          </a>
      </li>


      