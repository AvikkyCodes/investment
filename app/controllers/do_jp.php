<?php

Class Do_Jp extends Controller
{

	public function index($amount="", $plan="")
	{
		$message = 'Congratulations! You have successfully subscribed to the ' . ucfirst($plan) . ' Investment Plan.'; 
        $message_type = SUCCESS_MESSAGE;
        $redirect = 'my-investments';
        $days = []; $endDate = '';
        $startDate = date("F d, Y", strtotime("+1 day"));
        $interest = $profit = 0;
        $serial = generate_serial_number();

		try 
		{
            if (UserAccount::hasInsufficientFunds(getUserId(), $amount)) 
            {
                $message = 'Sorry, your account balance is too low to subscribe to this plan. Fund your account to continue.';
                $message_type = ERROR_MESSAGE;
                $redirect = 'join-plan';
                $description = '';
            } 
            else 
            {
                if ($plan == 'bronze') 
                {
                    $endDate = date("F d, Y", strtotime("+14 days"));
                    $days = generateDays(14);
                    $interest = (1.5/100) * (int) $amount;
                    $profit = $interest * 14;
                } 
                elseif ($plan == 'standard') 
                {
                    $endDate = date("F d, Y", strtotime("+7 days"));
                    $days = generateDays(7);
                    $interest = (2.0/100) * (int) $amount;
                    $profit = $interest * 7;
                } 
                else 
                {
                    $endDate = date("F d, Y", strtotime("+14 days"));
                    $days = generateDays(14);
                    $interest = (2.5/100) * (int) $amount;
                    $profit = $interest * 14;
                }
                
                UserInvestment::joinPlan($serial, $plan, $amount, $interest, $profit, $startDate, $endDate);
                InvestmentTracking::register($serial, $interest, $days);
                UserAccount::updateAccountBalance('debit', getUserId(), $amount);
                $description = 'Subscribed to the '. ucfirst($plan). ' Investment Plan.';
                UserTransaction::makeTransaction(getUserId(), 'investment', $description, $amount); 
                registerLog(ACTIVITY_LOG, getUserEmail().' subscribed to the '.ucfirst($plan).' with $'.$amount);
               
            }
			
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}


		setViewMessage($message, $message_type);
        redirectTo($redirect);
	}

	
} 

