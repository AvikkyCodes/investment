<?php

Class Withdraw extends Controller
{

	public function index()
	{
        $msg = getViewMessage();

		$this->view('withdraw',
					[
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

