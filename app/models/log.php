<?php

class Log extends Model
{
	
	//register an event in the logs table
	protected static function register($type, $description)
	{
		Log::create([
			'type' 						=> $type,
			'description' 				=> $description,
			'user_id' 					=> getUserId(),
			'timestamp' 				=> logTimestamp()
		]);
	}

	//activity logs
	protected static function activity()
	{
		$logs = Log::where('type', '=', 'activity')->orderBy('id', 'desc')->get();
		return $logs;
	}

	//error logs
	protected static function error()
	{
		$logs = Log::where('type', '=', 'error')->orderBy('timestamp', 'desc')->get();
		return $logs;
	}

	//warning logs
	protected static function warning()
	{
		$logs = Log::where('type', '=', 'warning')->orderBy('created_at', 'desc')->get();
		return $logs;
	}


	//user logs
	protected static function userLogs($id)
	{
		$logs = Log::where('user_id', '=', $id)
		->where('type', '=', 'activity')->orderBy('created_at', 'desc')
		->get();
		return $logs;
	}




	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}