<?php

Class Payment extends Controller
{

	public function index()
	{
		if (checkViewData('payment')) 
		{
			$data = getViewData('payment');
			$amount = $data->amount;
			$paymentMethod = $data->paymentMethod;
			$msg = getViewMessage();
			$address = '';
	
			try 
			{
				$address = PaymentMethod::getAddress($paymentMethod);
			} 
			catch (Illuminate\Database\QueryException $e) 
			{
				$msg = checkDatabaseError($e);
				$message = $msg['message'];
				$message_type = $msg['message_type'];
			}
	
			$this->view('payment',
						[
							'message' => $msg['message'],
							'message_type' => $msg['message_type'],
							'method' => $paymentMethod,
							'amount' => $amount,
							'address' => $address
						]
					);
		}
		else 
		{
			redirectTo('dashboard');
		}
		
	}
	
} 

