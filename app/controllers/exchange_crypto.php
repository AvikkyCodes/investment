<?php

Class Exchange_Crypto extends Controller 
{

	public function index($source="", $destination="", $amount="")
	{
        $message = 'Crypto exchange successful.';
		$message_type = SUCCESS_MESSAGE; 
		$result = 0;
		$rates = [
			'btc' => 0.000051, //1 USD
			'link' => 0.14,
			'bnb' => 0.0037,
			'ada' => 2.15,
			'xrp' => 3.07,
			'ltc' => 0.017,
			'bch' => 0.0085,
			'eth' => 0.00067,
			'usdt' => 1.0,
		];

		try
		{
			if ($source !== $destination) 
			{
				if ($source === 'usd') 
				{
					if (!UserAccount::hasInsufficientFunds(getUserId(), $amount)) 
					{ 
						$result = $amount * $rates[$destination]; 
						UserAccount::updateAccountBalance('debit', getUserId(), $amount);
						UserCrypto::updateUserCrypto('credit', getUserId(), $destination, $result);
						UserTransaction::makeTransaction(getUserId(), 'Crypto Exchange', 
						'Exchanged ' .number_format($amount). ' ' .strtoupper($source). ' for ' .$result. ' ' .strtoupper($destination), 
						$amount);
					}
					else 
					{
						$message = 'Sorry, your account balance is too low for this transaction.';
						$message_type = ERROR_MESSAGE;
					}
				} 
				elseif ($destination === 'usd') 
				{
					if (!UserCrypto::hasInsufficientFunds(getUserId(), $source, $amount)) 
					{ 
						$result = $amount / $rates[$source]; 
						UserAccount::updateAccountBalance('credit', getUserId(), $result);
						UserCrypto::updateUserCrypto('debit', getUserId(), $source, $amount);
						UserTransaction::makeTransaction(getUserId(), 'Crypto Exchange', 
						'Exchanged ' .$amount. ' ' .strtoupper($source). ' for ' .number_format($result). ' ' .strtoupper($destination), 
						$amount);
					}
					else 
					{
						$message = 'Sorry, your ' . strtoupper($source) . ' balance is too low for this transaction.';
						$message_type = ERROR_MESSAGE;
					}
				} 
				else 
				{
					if (!UserCrypto::hasInsufficientFunds(getUserId(), $source, $amount)) 
					{ 
						$to_usd = $amount / $rates[$source];
						$result = $to_usd * $rates[$destination]; 
						UserCrypto::updateUserCrypto('credit', getUserId(), $destination, $result);
						UserCrypto::updateUserCrypto('debit', getUserId(), $source, $amount);
						UserTransaction::makeTransaction(getUserId(), 'Crypto Exchange', 
						'Exchanged ' .$amount. ' ' .strtoupper($source). ' for ' .$result. ' ' .strtoupper($destination), 
						$amount);
					}
					else 
					{
						$message = 'Sorry, your ' . strtoupper($source) . ' balance is too low for this transaction.';
						$message_type = ERROR_MESSAGE;
					}
					
				}
			} 
			else 
			{
				$message = 'Source and destination account cannot be the same.';
				$message_type = ERROR_MESSAGE;
			}
			

			
		}
		catch (Illuminate\Database\QueryException $e)
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		
        setViewMessage($message, $message_type);
        redirectTo('crypto-exchange');
		
	}


}