<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
    $page_title = 'Make Transactions'; 
    require_once 'resources/header.php';
  ?>

  <style>
    #form-div {
        padding: 10px;
    }
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

   <?php require_once 'resources/sidebar.php'; ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
        <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <div class="row">

              <!-- Area Chart -->
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">
                        Make Transactions
                    </h1>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">

                    <?php require_once 'resources/form_sub_msg.php';?>    
                  
                    <div class="row">
                      
                        <div class="col-md-5">
                            <div class="form-vertical" id="form-div">
                        
                                <form method="POST" class="form-vertical" action="simulate-transaction" >
                                    
                                    <div class="form-group">
                                      <label>Select Action:</label>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type" value="deposit" id="flexRadioDefault1" checked>
                                        <label class="form-check-label text-lg text-bolder text-dark" for="flexRadioDefault1">
                                          Deposit
                                        </label>
                                      </div>
                                      <div class="form-check mt-2">
                                        <input class="form-check-input" type="radio" name="type" value="withdrawal" id="flexRadioDefault2">
                                        <label class="form-check-label text-lg text-bolder text-dark" for="flexRadioDefault2">
                                          Withdrawal
                                        </label>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Enter Name:</label>
                                        <input type="text" class="form-control" name="name" placeholder="Enter full name" required
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label>Enter Amount:</label>
                                        <input type="number" class="form-control" name="amount" placeholder="Enter amount to deposit" required
                                        >
                                    </div>

                                    <div class="form-group mr-2 mt-2"> 
                                        <button type="submit" class="btn btn-primary btn-lg">Proceed</button>
                                    </div>

                                </form>
                            
                            </div>
                        </div>

                        <div class="col-md-7">
                          
                        </div>

                    </div>
                    <!-- end of row -->
                    <hr>
                    <div class="row">
                      <div class="col-md-12">
                        <iframe scrolling="no" allowtransparency="true" width="100%" height="650" src="<?=THIS_SERVER . '/'. 'recent-transactions' ?>" frameborder="0">
                        </iframe>
                      </div>
                    </div>
                  </div>
                 </div>
               </div>
             </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php';
      ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->


</body>

</html>
