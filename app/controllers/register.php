<?php

    class Register extends Controller
    {

        public function index()
        {
            $ref_id = '';

            if (is_logged_in()) 
            {
                redirectTo('dashboard');
            } 
            else 
            {
                $msg = getViewMessage();

                if (checkViewData('referral')) 
                {
                    $data = getViewData('referral');
                    $ref_id = $data->ref_id;
                }

                $this->view("register", 
                    [
                        'ref_id' => $ref_id,
                        'message' => $msg['message'],
                        'message_type' => $msg['message_type']
                    ]
                );
            }
            
        }
        

    }