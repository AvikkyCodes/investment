<?php

class EmailVerification extends Model
{
	
	protected static function register($email, $verification_code, $expire)
	{
		EmailVerification::create([
			'email' 					=> $email,
			'verification_code' 		=> encryptPassword($verification_code),
			'expire' 				    => $expire
		]);
	}

	protected static function getByEmail($email)
	{
		$data = EmailVerification::where('email', '=', $email)->first();
		return $data;
	}

	protected static function verifyEmail($email)
	{
		EmailVerification::where('email', '=', $email)->update(['status' => true]);
	}

	protected static function userEmailExists($email)
	{
		return EmailVerification::where('email', '=', $email)->exists() ? true : false;
	}

	protected static function updateEmailVerification($email, $code, $expire)
	{
		EmailVerification::where('email', '=', $email)->update([
			'verification_code' => encryptPassword($code), 'expire' => $expire
		]);
	}






	//relationships

	


}