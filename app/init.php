<?php
//
//The init file requires all necessary files
//needed to run the app successfully.
//
//session start
require_once "controllers/resources/session.php";
//composer autoloader
require_once "vendor/autoload.php";
//main config file
require_once "config.php";
//app config file
require_once "app/config.php";
//app core, handles routing
require_once "core/app.php";
//base controller class
require_once "core/controller.php";
//base model class
require_once "core/model.php";
//general functions
require_once "controllers/resources/included_functions.php";
//database
require_once "app/database/database.php";
//php mailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once "phpmailer/PHPMailer.php";
require_once "phpmailer/SMTP.php";
require_once "phpmailer/Exception.php";
