<?php

Class User_Logs extends Controller
{

	public function index()
	{
		$message = $message_type = '';

		try 
		{
			$logs = Log::userLogs(getUserId());	
		} 
		catch (Illuminate\Database\QueryException $e) 
		{
			$msg = checkDatabaseError($e);
			$message = $msg['message'];
			$message_type = $msg['message_type'];
		}
		

		$this->view("user_logs", 
			[
				'logs' 		=> $logs,
				'message' 		=> $message,
				'message_type' 	=> $message_type
			]
		);
	}
	
} 

