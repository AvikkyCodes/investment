<?php

Class Investment_Details extends Controller
{

	public function index()
	{
		if (checkViewData('investment')) 
		{
			$message = ''; $message_type = ERROR_MESSAGE;
			$data = getViewData('investment');
			$investment = UserInvestment::getBySerialNumber($data->serial);
			$tracking = InvestmentTracking::getInvestmentTrackings($data->serial);


			$this->view('investment_details',
				[
					'message' 		=> $message,
					'message_type'	=> $message_type,
					'investment'	=> $investment,
					'tracking'		=> $tracking
				]
			);
		} 
		else 
		{
			redirectTo('dashboard');
		}
		
		
	}
	
} 

