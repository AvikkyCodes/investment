<?php

Class Wallets extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }

        $msg = getViewMessage();
		
		$btc = PaymentMethod::getAddress('btc');
		$eth = PaymentMethod::getAddress('eth');

		$this->view('wallets',
			[
				'btc' => $btc,
				'eth' => $eth,
				'message' => $msg['message'],
				'message_type' => $msg['message_type']
			]
		);
	}
}

?>