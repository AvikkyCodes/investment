<!DOCTYPE html>
<html>
<head>
	<?php 
		$page_title = 'Landing';
		include_once "resources/header.php"; 
	?>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="text-center mt-5" >

					<h1 class="mb-3">
						Welcome!
					</h1>

					<img src="../../img/ovaboss_200x200.png" class="img-fluid mb-3">

				<?php 
					if (!is_null($data['default_password']) || !empty($data['default_password'])) :
				?>
					<div class="mt-2 pb-4">
						<h1 class="">Attention!</h1>
						<h3>
							Change password from the default, to ensure maximum account security.
						</h3>
						<a href="change-password-form" class="btn btn-danger btn-user btn-block btn-icon-split p-1 mt-2">
							<span class="mr-2 p-1">CHANGE PASSWORD NOW</span>
							<span class="icon text-white">
	                        <i class="fas fa-key"></i>
	                      	</span>
						</a>
					</div>

				<?php endif; ?>

					<a href="dashboard" class="btn btn-success btn-user btn-block btn-icon-split p-1 mt-3">
                      <span class="mr-2 p-1">PROCEED TO DASHBOARD</span>
                      <span class="icon text-white">
                        <i class="fas fa-arrow-alt-circle-right"></i>
                      </span>
                    </a>

				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</body>
</html>