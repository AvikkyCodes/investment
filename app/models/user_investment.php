<?php

class UserInvestment extends Model
{
	protected static function joinPlan($serial, $plan, $amount, $interest, $profit, $start_date, $end_date)
	{
		UserInvestment::create([
			'serial_number' 			=> $serial,
			'user_id' 					=> getUserId(),
			'plan' 					    => $plan,
			'amount' 					=> $amount,
			'interest' 					=> $interest,
			'profit' 			        => $profit,
			'start_date' 				=> $start_date,
			'end_date' 				    => $end_date,
			'date_created'				=> logTimestamp()
		]);
	}

	protected static function hasInvestment($id) 
	{
		return UserInvestment::where('user_id', '=', $id)->count() > 0 ? true : false;
	}

	protected static function getUserInvestments($id)
	{
		$data = UserInvestment::where('user_id', '=', $id)->orderBy('created_at', 'desc')->get();
		return $data;
	}

	protected static function getOpenInvestments()
	{
		$data = UserInvestment::where('status', '=', 'open')->get();
		return $data;
	}

	protected static function getOpenUserInvestments($id)
	{
		$data = UserInvestment::where('user_id', '=', $id)
		->where('status', '=', 'open')
		->orderBy('created_at', 'desc')->get();
		return $data;
	}

	protected static function getClosedUserInvestments($id)
	{
		$data = UserInvestment::where('user_id', '=', $id)
		->where('status', '=', 'closed')
		->orderBy('created_at', 'desc')->get();
		return $data;
	}

    protected static function closeInvestment($id)
	{
		UserInvestment::where('id', '=', $id)->update(['status' => 'closed']);
	}


	protected static function getBySerialNumber($serial)
	{
		$data = UserInvestment::where('serial_number', '=', $serial)->first();
		return $data;
	}


	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}