<?php

Class Account_Settings extends Controller
{

	public function index()
	{
        $msg = getViewMessage();

		$user = User::getById(getUserId());
		$settings = UserSetting::getUserSettings(getUserId());

		$this->view('account_settings',
					[
						'user' => $user,
						'settings' => $settings,
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

