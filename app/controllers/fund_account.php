<?php

Class Fund_Account extends Controller
{

	public function index()
	{
		if (!is_an_admin()) 
		{ redirectTo('dashboard'); }
		
        $msg = getViewMessage();

        $users = User::active();

		$this->view('fund_account',
					[
                        'users' => $users,
						'message' => $msg['message'],
						'message_type' => $msg['message_type']
					]
				);
	}
	
} 

