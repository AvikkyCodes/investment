<?php

//root directory
define('ROOT', __DIR__);

//directory separator
define('DS', DIRECTORY_SEPARATOR);

//server name and request scheme
//for forming absolute URLs.
define('THIS_SERVER', $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['SERVER_NAME']);


?>