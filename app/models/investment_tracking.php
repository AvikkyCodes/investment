<?php

class InvestmentTracking extends Model
{
	protected static function register($serial, $interest, $data)
	{
		$remarks = '';
        for ($i=1; $i <= count($data); $i++) 
        { 
			$remark = 'Day ' .$i. ' investment interest of $'.number_format($interest).' returned.';
			if ($i == count($data)) 
			{
				$remark = 'Day ' .$i. ' investment interest of $'.number_format($interest).' returned. Capital returned. Account credited.';
			}
            InvestmentTracking::create([
                'serial_number' 			=> $serial,
                'dateandtime' 				=> $data[$i],
                'remarks' 					=> $remark
            ]);
        }

	}

	protected static function getInvestmentTrackings($serial)
	{
		$data = InvestmentTracking::where('serial_number', '=', $serial)->get();
		return $data;
	}


	//relationships

	public function user() {
		return $this->belongsTo(User::class);
	}


}