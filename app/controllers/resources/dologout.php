<?php
	if (isset($_SESSION["id"])) 
	{
		$_SESSION = array();

		// var_dump($_COOKIE);

		// setcookie('login', '', time() - 4200);

		if (isset($_COOKIE[session_name()])) 
		{
			setcookie(session_name(), '', time() - 4200, '/');
		}

		foreach ($_COOKIE as $key => $value) 
		{
			setcookie($key, '', time() - 4200, '/');
		}

		session_unset();
		session_destroy();
	}

  	//unset login cookie
	if (isset($_COOKIE['login'])) 
	{
	    setcookie('login', '', time() - 4200, '/');
	}
	
?>