<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Crypto Exchange';
    require_once 'resources/header.php'; 
  ?>

  <style type="text/css">

    #tab {
      padding: 10px;
      margin: 10px;
    }

    .nav {
        padding: 10px;
    }

    .nlink {
        padding: 10px;
        margin: 20px;
        margin-top: 0px;
        border-radius: 5px;
        font-size: 25px;
        font-weight: bold;
        border: 1px solid black;
    }

  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		Crypto Exchange
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                <?php require_once 'resources/form_sub_msg.php'; ?>

                
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b>$<?=number_format($data['balance'])?></b></h5>
                                        <small class="text-light">Account Balance</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                  <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://img.icons8.com/color/48/000000/bitcoin--v1.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['btc']?> <br> BTC</b></h5>
                                        <small class="text-muted usdelement" id="btc"></small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        						                        <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://img.icons8.com/fluency/48/000000/ethereum.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['eth']?> <br> ETH</b></h5>
                                        <small class="text-muted usdelement" id="eth"></small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                                                                        <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://img.icons8.com/fluency/48/000000/litecoin.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['ltc']?> <br> LTC</b></h5>
                                        <small class="text-muted usdelement" id="ltc"></small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                                                                        <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://img.icons8.com/cotton/64/000000/chainlink.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['link']?> <br> LINK</b></h5>
                                        <small class="text-muted usdelement" id="link"></small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                                                <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://s2.coinmarketcap.com/static/img/coins/64x64/1839.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['bnb']?> <br> BNB</b></h5>
                                        <small class="text-muted usdelement" id="bnb"></small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                                                <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://s2.coinmarketcap.com/static/img/coins/64x64/2010.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['ada']?> <br> ADA</b></h5>
                                        <small class="text-muted usdelement" id="ada"></small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                         
                                                                        <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://img.icons8.com/color/48/000000/tether--v2.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['usdt']?> <br> USDT</b></h5>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                                                                        <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-75" src="https://img.icons8.com/material-sharp/24/000000/bitcoin.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['bch']?> <br> BCH</b></h5>
                                        <!-- <small class="text-muted usdelement" id="bch">$0</small> -->
                                    </div>
                                </div>
                            </div>
                        </div>  
                                                                        <div class="col-sm-6 col-lg-3">
                            <div class="p-3 card bg-dark shadow">
                                <div class="d-flex align-items-center">
                                    <span class="mr-3 bg-transparent stamp stamp-md">
                                        <img class="w-50" src="https://img.icons8.com/fluency/48/000000/ripple.png">
                                    </span>
                                    <div>
                                        <h5 class="mb-1 text-light"><b><?=$data['crypto']['xrp']?> <br> XRP</b></h5>
                                        <!-- <small class="text-muted usdelement" id="xrp">$0</small> -->
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="tradingview-widget-container">
                                <div id="tradingview_f933e"><div id="tradingview_fe43e-wrapper" style="position: relative;box-sizing: content-box;width: 100%;height: calc(400px - 32px);margin: 0 auto !important;padding: 0 !important;font-family: -apple-system, BlinkMacSystemFont, 'Trebuchet MS', Roboto, Ubuntu, sans-serif;"><div style="width: 100%;height: calc(400px - 32px);background: transparent;padding: 0 !important;"><iframe id="tradingview_fe43e" src="https://s.tradingview.com/widgetembed/?frameElementId=tradingview_fe43e&amp;symbol=COINBASE%3ABTCUSD&amp;interval=1&amp;hidesidetoolbar=0&amp;symboledit=1&amp;saveimage=1&amp;toolbarbg=f1f3f6&amp;studies=BB%40tv-basicstudies&amp;theme=dark&amp;style=9&amp;timezone=Etc%2FUTC&amp;studies_overrides=%7B%7D&amp;overrides=%7B%7D&amp;enabled_features=%5B%5D&amp;disabled_features=%5B%5D&amp;locale=en&amp;utm_source=orchardcapitaltraders.com&amp;utm_medium=widget_new&amp;utm_campaign=chart&amp;utm_term=COINBASE%3ABTCUSD" style="width: 100%; height: 100%; margin: 0 !important; padding: 0 !important;" allowtransparency="true" scrolling="no" allowfullscreen="" frameborder="0"></iframe></div></div></div>
                                <div class="tradingview-widget-copyright" style="width: 100%;">
                                    <a href="#" rel="noopener" target="_blank"><span class="blue-text"></span> <span class="blue-text">Personal trading chart</span></a>
                                </div>                 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <form method="POST" action="exchange-crypto" id="exchnageform">
                                <div class="form-group">
                                    <h5 class="text-dark">Source Account</h5>
                                    <select class="form-control text-light bg-dark" name="source" id="sourceasset">
                                        <option value="btc">BTC</option> 
                                            <option value="link">LINK</option> 
                                            <option value="bnb">BNB</option> 
                                            <option value="ada">ADA</option> 
                                            <option value="xrp">XRP</option> 
                                            <option value="ltc">LTC</option> 
                                            <option value="bch">BCH</option> 
                                            <option value="eth">ETH</option> 
                                            <option value="usdt">USDT</option> 
                                        <option value="usd">USD</option> 
                                    </select>
                                </div>
                  
                                <div class="form-group">
                                    <h5 class="text-dark">Destination Account</h5>
                                    <select name="destination" class="form-control text-light bg-dark" id="destinationasset">
                                        <option value="usd">USD</option> 
                                        <option value="btc">BTC</option> 
                                        <option value="link">LINK</option> 
                                        <option value="bnb">BNB</option> 
                                        <option value="ada">ADA</option> 
                                        <option value="xrp">XRP</option> 
                                        <option value="ltc">LTC</option> 
                                        <option value="bch">BCH</option> 
                                        <option value="eth">ETH</option> 
                                        <option value="usdt">USDT</option> 
                                    </select>
                                    <small class="text-dark">NOTE:USD is your account balance.</small>
                                </div>
                                
                                <div class="form-group">
                                    <h5 class="text-dark">Amount</h5>
                                    <input type="text" name="amount" class="form-control text-light bg-dark" placeholder="Enter amount to exchange" id="amount">
                                </div>
                                <!-- <div class="form-group">
                                    <h5 class="text-dark">You will get</h5>
                                    <input type="text" class="form-control text-light bg-dark" placeholder="Quantity of usd" id="quantity">
                                </div> -->
                                <input type="hidden" id="realquantity" name="quantity">
                                <div class="form-group">
                                    <span class="mb-1 ml-2 caption">
                                        <span class="text-dark"><b>Fees = 2%</b></span>
                                    </span>
                                </div>
                  
                                <div class="cta inline-group">
                                    <button class="btn btn-success btn-block btn-sm" disbaled="">
                                        Exchange
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                
            </div>
          </div>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>