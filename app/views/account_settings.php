<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Account Settings';
    require_once 'resources/header.php'; 
  ?>

  <style type="text/css">

    #tab {
      padding: 10px;
      margin: 10px;
    }

    .nav {
        padding: 10px;
    }

    .nlink {
        padding: 10px;
        margin: 20px;
        margin-top: 0px;
        border-radius: 5px;
        font-size: 15px;
        font-weight: bold;
        border: 1px solid black;
    }

    #form-div {
        padding: 20px;
    }

    textarea {
        max-height: 70px;
        min-height: 70px;
        max-width: 400px;
    }

  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			<div class="row">

                <div class="col-md-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                        <h1 class="m-0 font-weight-bold text-primary">

                            Account Settings
                        </h1>
                    </div>

                    <!-- Card Body -->
                    <div class="card-body">

                        <ul class="nav nav-tabs">
                            <?php if(!is_an_admin()) :?>
                            <li class="nlink"><a data-toggle="tab" href="#home">Personal Settings</a></li>
                            <li class="nlink"><a data-toggle="tab" href="#menu1">Withdrawal Settings</a></li>
                            <?php endif; ?>
                            <li class="nlink"><a data-toggle="tab" href="#menu2">Password/Security</a></li>
                            <!-- <li class="nlink"><a data-toggle="tab" href="#menu3">Other Settings</a></li> -->
                        </ul>

                        <div class="tab-content">

                            <?php require_once 'resources/form_sub_msg.php';?>

                            <?php if(!is_an_admin()) :?>
                            <div id="home" class="tab-pane active" >
                                <h2 class="text-center mt-2">PERSONAL SETTINGS</h2>
                                <div class="form-vertical " id="form-div">
                      
                                    <form method="POST" class="form-vertical" action="update-settings" >

                                        <div class="form-row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Full Name:</label>
                                                    <input type="text" class="form-control" name="full_name" value="<?=$data['user']['full_name']?>" required >
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Email:</label>
                                                    <input type="email" class="form-control" name="email" value="<?=$data['user']['email']?>" required readonly >
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Phone Number:</label>
                                                    <input type="text" class="form-control" name="phone" value="<?=$data['user']['phone']?>" required >
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Date of Birth:</label>
                                                    <input type="date" class="form-control" name="dob" value="<?=$data['user']['dob']?>" required >
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Nationality:</label>
                                            <textarea class="form-control" name="address" required cols="10" rows="1" placeholder="Full Address" ><?=$data['user']['address']?></textarea>
                                        </div>

                                        <div class="form-group mr-2 mt-2">
                                            <button type="submit" class="btn btn-primary btn-lg">Update Profile</button> 
                                        </div>

                                    </form>
                                    
                                </div>
                            </div>


                            <!-- WITHDRAWALS -->
                            <div id="menu1" class="tab-pane fade">
                                <h2 class="text-center mt-2">WITHDRAWAL SETTINGS</h2>
                                <div class="form-vertical " id="form-div">
                      
                                    <form method="POST" class="form-vertical" action="update-settings/withdrawal" >
                                        <h2 class="mb-2">Bank Account</h2>
                                        <div class="form-row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Bank Name:</label>
                                                    <input type="text" class="form-control" name="bank_name" value="<?=$data['settings']['bank_name']?>" placeholder="Enter Bank Name">
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Account Name:</label>
                                                    <input type="text" class="form-control" name="account_name" value="<?=$data['settings']['account_name']?>" placeholder="Enter Account Name" >
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Account Number:</label>
                                                    <input type="text" class="form-control" name="account_number" value="<?=$data['settings']['account_number']?>" placeholder="Enter Account Number">
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Swift Code:</label>
                                                    <input type="text" class="form-control" name="swift_code" value="<?=$data['settings']['swift_code']?>" placeholder="Enter Swift Code">
                                                </div>
                                            </div>
                                        </div>

                                        <h2 class="mb-2 mt-2">Cryptocurrency</h2>
                                        <div class="form-row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Bitcoin:</label>
                                                    <input type="text" class="form-control" name="btc_address" value="<?=$data['settings']['btc_address']?>" required placeholder="Enter Bitcoin Address">
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Ethereum:</label>
                                                    <input type="text" class="form-control" name="eth_address" value="<?=$data['settings']['eth_address']?>" placeholder="Enter Ethereum Address" >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Litecoin:</label>
                                            <input type="text" class="form-control" name="ltc_address" value="<?=$data['settings']['ltc_address']?>" placeholder="Enter Litecoin Address" style="width:410px">
                                        </div>

                                        <div class="form-group mr-2 mt-2">
                                            <button type="submit" class="btn btn-primary btn-lg">Save</button> 
                                        </div>

                                    </form>
                                    
                                </div>
                            </div>
                            <?php endif; ?>
                            
                            
                            <div id="menu2" class="tab-pane <?=is_an_admin() ? 'active' : 'fade'; ?>" >
                                <h2 class="text-center mt-2">PASSWORD/SECURITY</h2>
                                <div class="form-vertical" id="form-div">
                      
                                    <form method="POST" class="form-vertical" action="change-password" >

                                    <div class="form-group alert alert-secondary mr-2 mb-4">
                                        <h6 class="text-dark">
                                        <strong>
                                            Password requirements: 
                                            <ul>
                                            <li>Password must be six(6) to twelve(12) characters long </li>
                                            </ul>
                                        </strong>
                                        </h6>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Old Password:</label>
                                        <input type="password" class="form-control w-50" name="currentPassword" required
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label>New Password:</label>
                                        <input type="password" class="form-control w-50" name="newPassword" maxlength="12" minlength="6" required >
                                    </div>

                                    <div class="form-group">
                                        <label>Confirm New Password:</label>
                                        <input type="password" class="form-control w-50" name="rNewPassword" maxlength="12" minlength="6" required >
                                    </div>

                                    <div class="form-group mr-2 mt-2">
                                        <input type="submit" class="btn btn-primary btn-lg form-control w-50" value="Update Password"> 
                                    </div>

                                    </form>
                                    
                                </div>
                            </div>


                            <!-- OTHERS -->
                            <!-- <div id="menu3" class="tab-pane fade">
                            <h2 class="text-center mt-2">OTHER SETTINGS</h2>
                                
                            </div> -->
                            
                        </div>
                    
                    </div>
                </div>
            </div>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>