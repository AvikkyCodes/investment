<?php

    class Index extends Controller
    {

        public function index()
        {  
            if (is_logged_in()) 
            {
                redirectTo('dashboard');
            }
            else
            {
                init_email_sent();
                
                $this->view("index", 
                    [
                        'message' => '',
                        'message_type' => ''
                    ]
                );
            }
        }
        

    }

?>