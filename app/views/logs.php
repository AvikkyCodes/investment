<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
    $page_title = 'Logs';
    require_once 'resources/header.php'; 
    require_once 'resources/table_style.php'; 
  ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once 'resources/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Navbar -->
            <?php require_once 'resources/navbar.php'; ?>
        <!-- End of Navbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

			    <div class="row">

            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

                  	<h1 class="m-0 font-weight-bold text-primary">

                  		Activity Logs
              		  </h1>
                </div>

                <!-- Card Body -->
                <div class="card-body">

                <?php require_once 'resources/form_sub_msg.php'; ?>

                <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                            
                                    <thead>
                                    <tr id="mytable">
                                        <th id="mytable">Description</th>
                                        <th id="mytable">User</th>
                                        <th id="mytable">Timestamp</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php 
                                        foreach ($data['logs'] as $log):
                                    ?>
                                        <tr id="mytable">

                                            <td id="mytable">
                                                <?=$log->description?>
                                            </td>

                                            <td id="mytable">
                                                <?=User::getEmail($log->user_id)?>
                                            </td>

                                            <td id="mytable">
                                                <?=$log->timestamp?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                    
                  
                </div>
            </div>
          </div>

 			</div>

 		 </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once 'resources/footer.php'; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

</body>

</html>